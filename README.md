# mars rover kata

## index
* [project structure](#project-structure)
  * [packages](#packages)
* [project setup](#project-setup)
  * [development environment setup](#development-environment-setup)
  * [running tests](#running-tests)
* [mission control](#mission-control)
  * [mission control CLI](#mission-control-cli)
  * [examples](#examples)


## project structure
The project is developed as a monorepo using [pnpm workspaces](https://pnpm.js.org/en/workspaces)

## packages
The code is split into six packages:

* [@mars-rover/geometry](/packages/geometry)
* [@mars-rover/result](/packages/result)
* [@mars-rover/parser-combinators](/packages/parser-combinators)
* [@mars-rover/parser](/packages/parser)
* [@mars-rover/mission-control](/packages/mission-control)
* [@mars-rover/mission-control-cli](/packages/mission-control-cli)

## project setup
### development environment setup

* Install [pnpm](https://pnpm.js.org/):
  ```
  $ npm install -g pnpm
  ```
* Install dependencies:
  ```
  $ pnpm install && pnpm install -r
  ```

### running tests

To run the tests in every project in the workspace run

```
$ pnpm test
```

## mission control
The mission control is the interface to the user to run the code.

### mission control CLI
Running the mission control is as simple as
```
$ ./mctrl -f /path/to/input/file
```
If the mission control successfully parses the input file, the rover starts executing the commands received and log the results of those commands to a log file: `rover_log_{TIMESTAMP}.log`

If the mission control is not able to parse the input file it will log an error message in the console.

The mission control CLI will print the help message if the file option or the file path is missing or if run with the `-h` option:

```
$ ./mctrl -h

Mars Rover | Mission Control CLI

USAGE
  $ ./mctrl -f /path/to/input/file

OPTIONS
  -h, --help    Show this message
  -f FILE       Run the mission control with the input FILE
```

### examples
1.  ```
    $ ./mctrl -f data/input-examples/rover-test
    ```
    will create a log file (as described in the section above), with this content:
    ```
    O:1:0:E
    1:3:S
    0:1:W
    ```

2.  ```
    $ ./mctrl -f data/input-examples-with-syntax-errors/unknown-rover-command
    Expecting One of R, L, F, B, got 
    6 | Commands
    7 | RFF
    8 | RFG
          ^
    ```

3.  ```
    $ ./mctrl -f data/input-examples-with-syntax-errors/missing-trailing-new-line
    Expecting New line at the end of file, got 
    8 | RF
    9 | LFRFFLFFFLL
        ^
    ```
