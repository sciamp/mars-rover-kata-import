import { createWriteStream, WriteStream } from 'fs';

class Logger {
  private stream: WriteStream;

  constructor() {
    const timestamp = Date.now();
    this.stream = createWriteStream(`./rover_log_${timestamp}.log`);
  }

  public writeLine(chunk: string) {
    this.stream.write(`${chunk}\n`);
  }
}

export { Logger };
