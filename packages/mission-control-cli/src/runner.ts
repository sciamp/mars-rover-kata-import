import { readFile } from 'fs';
import { MissionControl } from '@mars-rover/mission-control';
import { parse } from '@mars-rover/parser';
import { Logger } from './logger';

export const missionControlRunner = (filePath: string) => {
  readFile(filePath, (error, data) => {
    if (error) {
      console.error(error.message);
      return;
    }

    const input = data.toString();

    parse(input).match(
      ({
        value: {
          gridSection,
          roverSection: { commands },
        },
      }) => {
        const missionControl = new MissionControl(gridSection, commands);

        const logger = new Logger();
        // eslint-disable-next-line no-restricted-syntax
        for (const report of missionControl.run()) {
          logger.writeLine(report);
        }
      },
      ({ expected, actual }) =>
        console.error(`Expecting ${expected}, got ${actual}`)
    );
  });
};
