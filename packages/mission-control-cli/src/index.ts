import { showHelp } from './help';
import { missionControlRunner } from './runner';

const option = process.argv[2];
const fileName = process.argv[3];

if (option !== '-f' || !fileName) {
  console.log(showHelp());
} else {
  missionControlRunner(fileName);
}
