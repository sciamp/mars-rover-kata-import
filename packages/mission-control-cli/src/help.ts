export const showHelp = (): string => `
Mars Rover | Mission Control CLI

USAGE
  $ ./mctrl -f /path/to/input/file

OPTIONS
  -h, --help    Show this message
  -f FILE       Run the mission control with the input FILE
`;
