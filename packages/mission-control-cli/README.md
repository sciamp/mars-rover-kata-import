# @mars-rover/mission-control-cli
The `@mars-rover/mission-control-cli` is just an interface for the `mctrl` CLI to run a mars rover mission. It takes care of loading the input file, parsing it and running the mission control.
