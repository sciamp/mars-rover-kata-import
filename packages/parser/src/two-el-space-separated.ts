import {
  bind,
  Context,
  Parser,
  ParserSuccess,
  stringParser,
  then,
} from '@mars-rover/parser-combinators';

const twoElSpaceSeparated = <T, S>(
  first: Parser<T>,
  second: Parser<S>
): Parser<{ first: T; second: S }> => {
  const spaceThenSecond = then(stringParser(' '), second);
  return bind(first, (firstValue: T) => (context: Context) =>
    spaceThenSecond(context).map(
      ({ value: secondValue, ...rest }: ParserSuccess<S>) => ({
        ...rest,
        value: { first: firstValue, second: secondValue },
      })
    )
  );
};

export { twoElSpaceSeparated };
