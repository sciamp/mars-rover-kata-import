import {
  stringParser,
  bind,
  Context,
  then,
  ParserSuccess,
  Success,
  Failure,
} from '@mars-rover/parser-combinators';
import { endOfLineParser } from './end-of-line-parser';

test('endOfLine parser successfully parses a new line', () => {
  expect(endOfLineParser({ input: '\nbar', index: 0 })).toEqual(
    Success({
      input: '\nbar',
      index: 1,
      value: '\n',
    })
  );
});

test('endOfLine parser successfully parses a sequence containing a new line', () => {
  const sequenceParser = bind(
    stringParser('foo'),
    (fooValue: string) => (context: Context) =>
      then(
        endOfLineParser,
        stringParser('bar')
      )(context).map(({ value, ...rest }: ParserSuccess<string>) => ({
        ...rest,
        value: [fooValue, '\n', value],
      }))
  );

  expect(sequenceParser({ input: 'foo\nbar', index: 0 })).toEqual(
    Success({
      input: 'foo\nbar',
      index: 7,
      value: ['foo', '\n', 'bar'],
    })
  );
});

test('endOfLine parser successfully parses a sequence ending with a new line', () => {
  const sequenceParser = bind(
    bind(stringParser('foo'), (fooValue: string) => (context: Context) =>
      then(
        stringParser(' '),
        stringParser('bar')
      )(context).map(({ value, ...rest }: ParserSuccess<string>) => ({
        ...rest,
        value: [fooValue, ' ', value],
      }))
    ),
    (value: string[]) => (context: Context) =>
      endOfLineParser(context).map(
        ({ index, input }: ParserSuccess<string>) => ({
          input,
          index,
          value: [...value, '\n'],
        })
      )
  );
  expect(sequenceParser({ input: 'foo bar\n', index: 0 })).toEqual(
    Success({
      input: 'foo bar\n',
      index: 8,
      value: ['foo', ' ', 'bar', '\n'],
    })
  );
});

test('endOfLine parser fails when parsing a non new line char', () => {
  expect(endOfLineParser({ input: 'foo bar', index: 0 })).toEqual(
    Failure({
      input: 'foo bar',
      index: 0,
      expected: '\n',
      actual: 'foo bar',
    })
  );
});
