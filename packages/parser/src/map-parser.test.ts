import { Failure, Success } from '@mars-rover/parser-combinators';
import { ItemType, mapParser } from './map-parser';

test('the map parser successfully parses the world definition', () => {
  const worldDefinition = `Grid
Size 4 5
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
`;

  expect(mapParser({ input: worldDefinition, index: 0 })).toEqual(
    Success({
      input: worldDefinition,
      index: 53,
      value: {
        size: {
          width: 4,
          height: 5,
        },
        items: [
          {
            type: ItemType.OBSTACLE,
            position: {
              x: 2,
              y: 0,
            },
          },
          {
            type: ItemType.OBSTACLE,
            position: {
              x: 0,
              y: 3,
            },
          },
          {
            type: ItemType.OBSTACLE,
            position: {
              x: 3,
              y: 2,
            },
          },
        ],
      },
    })
  );
});

test('the map parser successfully parses the world definition without obstacles', () => {
  const worldDefinition = `Grid
Size 4 5
`;

  expect(mapParser({ input: worldDefinition, index: 0 })).toEqual(
    Success({
      input: worldDefinition,
      index: 14,
      value: {
        size: {
          width: 4,
          height: 5,
        },
        items: [],
      },
    })
  );
});

test('the map parser fails when parsing the world definition without size', () => {
  const worldDefinition = `Grid
Obstacle 2 0
`;

  expect(mapParser({ input: worldDefinition, index: 0 })).toEqual(
    Failure({
      input: worldDefinition,
      index: 5,
      expected: 'Size width height',
      actual: `
1 | Grid
2 | Obstacle 2 0
    ^
`,
    })
  );
});

test('the map parser fails when parsing the world definition without the initial grid instruction', () => {
  const worldDefinition = `Size 4 5
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
`;

  expect(mapParser({ input: worldDefinition, index: 0 })).toEqual(
    Failure({
      input: worldDefinition,
      index: 0,
      expected: 'Grid section begin statement',
      actual: `
1 | Size 4 5
    ^
`,
    })
  );
});
