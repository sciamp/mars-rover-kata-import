import {
  endOfInputParser,
  run,
  bind,
  withError,
} from '@mars-rover/parser-combinators';
import { endOfLineParser } from './end-of-line-parser';
import { mapParser } from './map-parser';
import { roverCommandsParser } from './rover-commands-parser';

const parser = bind(
  bind(mapParser, gridSection => context =>
    roverCommandsParser(context).map(({ value: roverSection, ...rest }) => ({
      ...rest,
      value: { gridSection, roverSection },
    }))
  ),
  value => context =>
    endOfInputParser(context).map(({ input, index }) => ({
      input,
      index,
      value,
    }))
);

const withTrailingNewLineErrorMessage = withError(
  parser,
  ({ input, index, ...rest }) =>
    endOfInputParser({ input, index }).match(
      () =>
        endOfLineParser({ input, index: index - 1 }).match(
          () => ({ input, index, ...rest }),
          () => ({
            input,
            index,
            ...rest,
            expected: 'New line at the end of file',
          })
        ),
      () => ({ input, index, ...rest })
    )
);

export const parse = run(withTrailingNewLineErrorMessage);

export {
  Directions,
  MovementType,
  Rotation,
  Movement,
  RoverAction,
  RoverCommand,
  isMovement,
  isRotation,
  RoverSection,
} from './rover-commands-parser';

export { GridSection, GridItem, GridSize, ItemType } from './map-parser';
