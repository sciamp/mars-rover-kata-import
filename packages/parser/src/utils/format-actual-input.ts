import { getCursorPosition } from './get-cursor-position';

const formatActualInput = (
  input: string,
  index: number,
  numberOfLines: number = 3
): string => {
  const { lines, lineNumber: lastLineIndex, column } = getCursorPosition(
    input,
    index
  );

  const firstLineIndex = Math.max(0, lastLineIndex - numberOfLines);

  const padLength = `${lastLineIndex + 1}`.length;

  const reportLines = lines
    .slice(firstLineIndex, lastLineIndex)
    .map((line, i) => {
      const paddedLineNum = `${firstLineIndex + 1 + i}`.padStart(padLength);

      return `${paddedLineNum} | ${line}`;
    });

  const errorMarker = '^'.padStart(padLength + 3 + column, ' ');

  return ['', ...reportLines, errorMarker, ''].join('\n');
};

export { formatActualInput };
