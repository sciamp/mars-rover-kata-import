const countLines = (
  lines: string[],
  remaining: number,
  currentLineNumber: number
): { lineNumber: number; column: number } => {
  const [currentLine, ...remainingLines] = lines;

  if (!currentLine || currentLine.length > remaining)
    return { lineNumber: currentLineNumber, column: remaining + 1 };

  return countLines(
    remainingLines,
    remaining - currentLine.length - 1,
    currentLineNumber + 1
  );
};

const getCursorPosition = (
  input: string,
  index: number
): { lines: string[]; lineNumber: number; column: number } => {
  const lines = input.split('\n');
  return { lines, ...countLines(lines, index, 1) };
};

export { getCursorPosition };
