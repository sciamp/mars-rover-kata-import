import { getCursorPosition } from './get-cursor-position';
//             123456789
const input = `AAAAAAAAA
BBBBBBBBB
CCCCCCCCC
DDDDDDDDD
EEEEEEEEE
FFFFFFFFF
`;

test('getCursorPosition when index is in the beginning of the first line', () => {
  expect(getCursorPosition(input, 0)).toEqual({
    lines: input.split('\n'),
    lineNumber: 1,
    column: 1,
  });
});

test('getCursorPosition when index is in the middle of the first line', () => {
  expect(getCursorPosition(input, 3)).toEqual({
    lines: input.split('\n'),
    lineNumber: 1,
    column: 4,
  });
});

test('getCursorPosition when index is at the end of the first line', () => {
  expect(getCursorPosition(input, 9)).toEqual({
    lines: input.split('\n'),
    lineNumber: 2,
    column: 0,
  });
});

test('getCursorPosition when index is in the beginning of the second line', () => {
  expect(getCursorPosition(input, 10)).toEqual({
    lines: input.split('\n'),
    lineNumber: 2,
    column: 1,
  });
});

test('getCursorPosition when index is in the middle of the second line', () => {
  expect(getCursorPosition(input, 13)).toEqual({
    lines: input.split('\n'),
    lineNumber: 2,
    column: 4,
  });
});

test('getCursorPosition when index is at the end of the second line', () => {
  expect(getCursorPosition(input, 19)).toEqual({
    lines: input.split('\n'),
    lineNumber: 3,
    column: 0,
  });
});

test('getCursorPosition when index is in the beginning of the third line', () => {
  expect(getCursorPosition(input, 20)).toEqual({
    lines: input.split('\n'),
    lineNumber: 3,
    column: 1,
  });
});

test('getCursorPosition when index is in the middle of the third line', () => {
  expect(getCursorPosition(input, 23)).toEqual({
    lines: input.split('\n'),
    lineNumber: 3,
    column: 4,
  });
});

test('getCursorPosition when index is at the end of the third line', () => {
  expect(getCursorPosition(input, 29)).toEqual({
    lines: input.split('\n'),
    lineNumber: 4,
    column: 0,
  });
});

test('getCursorPosition when index is in the beginning of the last line', () => {
  expect(getCursorPosition(input, 50)).toEqual({
    lines: input.split('\n'),
    lineNumber: 6,
    column: 1,
  });
});

test('getCursorPosition when index is in the middle of the last line', () => {
  expect(getCursorPosition(input, 53)).toEqual({
    lines: input.split('\n'),
    lineNumber: 6,
    column: 4,
  });
});

test('getCursorPosition when index is at the end of the last line', () => {
  expect(getCursorPosition(input, 59)).toEqual({
    lines: input.split('\n'),
    lineNumber: 7,
    column: 0,
  });
});
