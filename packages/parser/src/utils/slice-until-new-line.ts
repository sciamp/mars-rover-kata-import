const sliceUntilNewLine = (input: string, index: number) => {
  const firstNewLineIndex = input.indexOf('\n', index);
  return firstNewLineIndex >= 0
    ? input.slice(index, firstNewLineIndex)
    : input.slice(index);
};

export { sliceUntilNewLine };
