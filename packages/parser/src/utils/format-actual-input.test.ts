import { formatActualInput } from './format-actual-input';

test('formatActualInput return a portion of input marking the current cursor position', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
RFF
RF
LFRFFLFFFLL
`;

  const formatted = formatActualInput(input, 37, 4);

  const expected = `
1 | Grid
2 | Size 5 4
3 | Obstacle 2 0
4 | Obstacle 0 3
              ^
`;
  expect(formatted).toBe(expected);
});

test('formatActualInput return a portion of input marking the current cursor position with default numberOfLines', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
RFF
RF
LFRFFLFFFLL
`;

  const formatted = formatActualInput(input, 37);

  const expected = `
2 | Size 5 4
3 | Obstacle 2 0
4 | Obstacle 0 3
              ^
`;
  expect(formatted).toBe(expected);
});

test('formatActualInput return a portion of input marking the current cursor position with more line than available', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
RFF
RF
LFRFFLFFFLL
`;

  const formatted = formatActualInput(input, 37, 100);

  const expected = `
1 | Grid
2 | Size 5 4
3 | Obstacle 2 0
4 | Obstacle 0 3
              ^
`;
  expect(formatted).toBe(expected);
});
