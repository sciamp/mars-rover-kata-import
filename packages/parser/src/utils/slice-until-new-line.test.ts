import { sliceUntilNewLine } from './slice-until-new-line';

test('the sliceUntilNewLine function behaves like String.prototype.slice() when string is a single line', () => {
  expect(sliceUntilNewLine('foo bar baz', 4)).toBe('bar baz');
});

test('the sliceUntilNewLine function behaves like String.prototype.slice() when string is a single line skipping any trailing new line', () => {
  expect(sliceUntilNewLine('foo bar baz\n', 4)).toBe('bar baz');
});

test('the sliceUntilNewLine slice a portion of the first of two lines', () => {
  expect(sliceUntilNewLine('foo bar baz\nsecond line here!', 4)).toBe(
    'bar baz'
  );
});

test('the sliceUntilNewLine slice a portion of the second of two lines', () => {
  expect(sliceUntilNewLine('first line here!\nfoo bar baz', 21)).toBe(
    'bar baz'
  );
});

test('the sliceUntilNewLine slice a portion of the second of two lines skipping any trailing new line', () => {
  expect(sliceUntilNewLine('first line here!\nfoo bar baz\n', 21)).toBe(
    'bar baz'
  );
});

test('the sliceUntilNewLine slice a portion of the second of three lines', () => {
  expect(
    sliceUntilNewLine('first line here!\nfoo bar baz\nthird line here!', 21)
  ).toBe('bar baz');
});
