import {
  bind,
  Context,
  Failure,
  many,
  oneOf,
  Parser,
  ParserSuccess,
  stringParser,
  Success,
  then,
  withError,
} from '@mars-rover/parser-combinators';
import { endOfLineParser } from './end-of-line-parser';
import { formatActualInput } from './utils/format-actual-input';

export enum Directions {
  FORWARD,
  BACKWARD,
  RIGHT,
  LEFT,
}

export enum MovementType {
  ROTATE,
  MOVE,
}

export type Rotation = {
  type: MovementType.ROTATE;
  direction: Directions.LEFT | Directions.RIGHT;
};

export type Movement = {
  type: MovementType.MOVE;
  direction: Directions.FORWARD | Directions.BACKWARD;
};

export interface RoverAction {
  type: MovementType;
  direction: Directions;
}

export const isRotation = (action: RoverAction): action is Rotation =>
  action.type === MovementType.ROTATE;

export const isMovement = (action: RoverAction): action is Movement =>
  action.type === MovementType.MOVE;

export type RoverCommand = RoverAction[];

export type RoverSection = {
  commands: RoverCommand[];
};

const turnRightCommandParser: Parser<RoverAction> = context =>
  stringParser('R')(context).map(({ input, index }: ParserSuccess<string>) => ({
    input,
    index,
    value: {
      type: MovementType.ROTATE,
      direction: Directions.RIGHT,
    },
  }));
const turnLeftCommandParser: Parser<RoverAction> = context =>
  stringParser('L')(context).map(({ input, index }) => ({
    input,
    index,
    value: { type: MovementType.ROTATE, direction: Directions.LEFT },
  }));
const moveForwardCommandParser: Parser<RoverAction> = context =>
  stringParser('F')(context).map(({ input, index }) => ({
    input,
    index,
    value: { type: MovementType.MOVE, direction: Directions.FORWARD },
  }));
const moveBackwardsCommandParser: Parser<RoverAction> = context =>
  stringParser('B')(context).map(({ input, index }) => ({
    input,
    index,
    value: { type: MovementType.MOVE, direction: Directions.BACKWARD },
  }));

const roverSectionParser = withError(
  then(stringParser('Commands'), endOfLineParser),
  ({ input, index }) => ({
    input,
    index,
    expected: 'Commands section begin statement',
    actual: formatActualInput(input, index),
  })
);

const actionParser: Parser<RoverAction> = oneOf(
  turnRightCommandParser,
  turnLeftCommandParser,
  moveForwardCommandParser,
  moveBackwardsCommandParser
);

const actionsParser: Parser<RoverAction[]> = many(actionParser);

const roverCommandEndOfLine = <T>(value: T): Parser<T> => (context: Context) =>
  endOfLineParser(context).map(({ input, index }) => ({
    input,
    index,
    value,
  }));

const commandsParser: Parser<RoverCommand[]> = bind(
  many(bind(actionsParser, roverCommandEndOfLine)),
  result => context =>
    actionParser(context).match(
      () =>
        actionsParser(context).match(
          ({ index }) =>
            roverCommandEndOfLine(result)({ ...context, index }).or(error =>
              Failure({
                ...error,
                expected: 'One of R, L, F, B',
                actual: formatActualInput(error.input, error.index),
              })
            ),
          ({ input, index }) =>
            Failure({
              input,
              index,
              expected: 'One of R, L, F, B',
              actual: formatActualInput(input, index),
            })
        ),
      () => Success({ ...context, value: result })
    )
);

export const roverCommandsParser: Parser<RoverSection> = (context: Context) =>
  then(
    roverSectionParser,
    commandsParser
  )(context).map(({ value, ...rest }) => ({
    ...rest,
    value: { commands: value },
  }));
