import {
  Failure,
  nonNegativeIntegerParser,
  NON_NEGATIVE_INTEGER,
  Parser,
  stringParser,
  Success,
} from '@mars-rover/parser-combinators';
import { twoElSpaceSeparated } from './two-el-space-separated';

test('the twoElSpaceSeparated parser parses two element separated by a space', () => {
  const parser: Parser<{ first: string; second: number }> = twoElSpaceSeparated(
    stringParser('FOO'),
    nonNegativeIntegerParser
  );

  expect(parser({ input: 'FOO 42', index: 0 })).toEqual(
    Success({ input: 'FOO 42', index: 6, value: { first: 'FOO', second: 42 } })
  );
});

test('the twoElSpaceSeparated parser fails if the first parser fails', () => {
  const parser: Parser<{ first: string; second: number }> = twoElSpaceSeparated(
    stringParser('FOO'),
    nonNegativeIntegerParser
  );

  expect(parser({ input: 'foo 42', index: 0 })).toEqual(
    Failure({ input: 'foo 42', index: 0, expected: 'FOO', actual: 'foo 42' })
  );
});

test('the twoElSpaceSeparated parser fails if the space is missing', () => {
  const parser: Parser<{ first: string; second: number }> = twoElSpaceSeparated(
    stringParser('FOO'),
    nonNegativeIntegerParser
  );

  expect(parser({ input: 'FOO42', index: 0 })).toEqual(
    Failure({ input: 'FOO42', index: 3, expected: ' ', actual: '42' })
  );
});

test('the twoElSpaceSeparated parser fails if the second parser fails', () => {
  const parser: Parser<{ first: string; second: number }> = twoElSpaceSeparated(
    stringParser('FOO'),
    nonNegativeIntegerParser
  );

  expect(parser({ input: 'FOO BAR', index: 0 })).toEqual(
    Failure({
      input: 'FOO BAR',
      index: 4,
      expected: NON_NEGATIVE_INTEGER,
      actual: 'BAR',
    })
  );
});
