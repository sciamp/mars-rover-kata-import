import { Failure, Success } from '@mars-rover/parser-combinators';
import {
  MovementType,
  Directions,
  roverCommandsParser,
} from './rover-commands-parser';

test('rover commands parser fails if first instruction is not "Commands"', () => {
  const commands = `RFF
RF
LFRFFLFFFLL
`;

  expect(
    roverCommandsParser({
      input: commands,
      index: 0,
    })
  ).toEqual(
    Failure({
      input: commands,
      index: 0,
      expected: 'Commands section begin statement',
      actual: `
1 | RFF
    ^
`,
    })
  );
});

test('rover commands parser successfully parses an empty list of commands', () => {
  const commands = 'Commands\n';

  expect(
    roverCommandsParser({
      input: commands,
      index: 0,
    })
  ).toEqual(
    Success({
      input: commands,
      index: 9,
      value: { commands: [] },
    })
  );
});

test('rover commands parser successfully parses a list of commands', () => {
  const commands = `Commands
RFF
RF
LFRFFLFFFLL
BB
`;

  expect(
    roverCommandsParser({
      input: commands,
      index: 0,
    })
  ).toEqual(
    Success({
      input: commands,
      index: 31,
      value: {
        commands: [
          [
            { type: MovementType.ROTATE, direction: Directions.RIGHT },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
          ],
          [
            { type: MovementType.ROTATE, direction: Directions.RIGHT },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
          ],
          [
            { type: MovementType.ROTATE, direction: Directions.LEFT },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
            { type: MovementType.ROTATE, direction: Directions.RIGHT },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
            { type: MovementType.ROTATE, direction: Directions.LEFT },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
            { type: MovementType.MOVE, direction: Directions.FORWARD },
            { type: MovementType.ROTATE, direction: Directions.LEFT },
            { type: MovementType.ROTATE, direction: Directions.LEFT },
          ],
          [
            { type: MovementType.MOVE, direction: Directions.BACKWARD },
            { type: MovementType.MOVE, direction: Directions.BACKWARD },
          ],
        ],
      },
    })
  );
});

test('rover commands parser fails if illegal action is found', () => {
  const commands = `Commands
RFF
RF
LFRFFLFFFLL
BBH
`;

  expect(
    roverCommandsParser({
      input: commands,
      index: 0,
    })
  ).toEqual(
    Failure({
      input: commands,
      index: 30,
      expected: 'One of R, L, F, B',
      actual: `
3 | RF
4 | LFRFFLFFFLL
5 | BBH
      ^
`,
    })
  );
});
