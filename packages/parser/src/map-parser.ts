import {
  bind,
  Context,
  Failure,
  many,
  nonNegativeIntegerParser,
  Parser,
  stringParser,
  Success,
  then,
  withError,
} from '@mars-rover/parser-combinators';
import { endOfLineParser } from './end-of-line-parser';
import { twoElSpaceSeparated } from './two-el-space-separated';
import { formatActualInput } from './utils/format-actual-input';

export enum ItemType {
  OBSTACLE,
}

export type GridItem = {
  type: ItemType;
  position: {
    x: number;
    y: number;
  };
};

export type GridSize = {
  width: number;
  height: number;
};

export type GridSection = {
  size: GridSize;
  items: GridItem[];
};

const twoIntegerSpaceSeparated = twoElSpaceSeparated(
  nonNegativeIntegerParser,
  nonNegativeIntegerParser
);

const stringAndTwoInteger = (match: string, expected: string) =>
  withError(
    bind(
      twoElSpaceSeparated(stringParser(match), twoIntegerSpaceSeparated),
      value => (context: Context) =>
        endOfLineParser(context).map(({ input, index }) => ({
          input,
          index,
          value,
        }))
    ),
    ({ input, index }) => ({
      input,
      index,
      expected,
      actual: formatActualInput(input, index),
    })
  );

const gridSectionBegin = withError(
  then(stringParser('Grid'), endOfLineParser),
  ({ input, index }) => ({
    input,
    index,
    expected: 'Grid section begin statement',
    actual: formatActualInput(input, index),
  })
);

const gridSizeParser = stringAndTwoInteger('Size', 'Size width height');

const obstaclesParser = bind(
  many(stringAndTwoInteger('Obstacle', 'Obstacle x y')),
  result => context =>
    stringParser('Obstacle')(context).match(
      ({ input, index }) =>
        Failure({
          input,
          index,
          expected: 'Obstacle x y',
          actual: formatActualInput(input, index),
        }),
      () => Success({ ...context, value: result })
    )
);

const mapParser = then(
  gridSectionBegin,
  bind(
    gridSizeParser,
    ({ second: { first: width, second: height } }): Parser<GridSection> => (
      context: Context
    ) =>
      obstaclesParser(context).map(({ input, index, value }) => ({
        input,
        index,
        value: {
          size: { width, height },
          items: value.map(({ second: { first, second } }) => ({
            type: ItemType.OBSTACLE,
            position: { x: first, y: second },
          })),
        },
      }))
  )
);

export { mapParser };
