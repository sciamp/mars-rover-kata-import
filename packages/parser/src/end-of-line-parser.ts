import { Parser, stringParser } from '@mars-rover/parser-combinators';

export const endOfLineParser: Parser<string> = stringParser('\n');
