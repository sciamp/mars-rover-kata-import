import { Failure, Success } from '@mars-rover/parser-combinators';
import { parse } from './index';
import { ItemType } from './map-parser';
import { MovementType, Directions } from './rover-commands-parser';

test('the mars rover parser successfully parse a valid input', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
RFF
RF
LFRFFLFFFLL
`;

  expect(parse(input)).toEqual(
    Success({
      input,
      index: 81,
      value: {
        gridSection: {
          size: {
            width: 5,
            height: 4,
          },
          items: [
            {
              type: ItemType.OBSTACLE,
              position: {
                x: 2,
                y: 0,
              },
            },
            {
              type: ItemType.OBSTACLE,
              position: {
                x: 0,
                y: 3,
              },
            },
            {
              type: ItemType.OBSTACLE,
              position: {
                x: 3,
                y: 2,
              },
            },
          ],
        },
        roverSection: {
          commands: [
            [
              { type: MovementType.ROTATE, direction: Directions.RIGHT },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
            ],
            [
              { type: MovementType.ROTATE, direction: Directions.RIGHT },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
            ],
            [
              { type: MovementType.ROTATE, direction: Directions.LEFT },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
              { type: MovementType.ROTATE, direction: Directions.RIGHT },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
              { type: MovementType.ROTATE, direction: Directions.LEFT },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
              { type: MovementType.MOVE, direction: Directions.FORWARD },
              { type: MovementType.ROTATE, direction: Directions.LEFT },
              { type: MovementType.ROTATE, direction: Directions.LEFT },
            ],
          ],
        },
      },
    })
  );
});

test('the mars rover fails to parse a valid input without leading newline', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
RFF
RF
LFRFFLFFFLL`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 80,
      expected: 'New line at the end of file',
      actual: `
 8 | RF
 9 | LFRFFLFFFLL
    ^
`,
    })
  );
});

test('the mars rover parser successfully parse a valid input with empty commands', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
`;

  expect(parse(input)).toEqual(
    Success({
      input,
      index: 62,
      value: {
        gridSection: {
          size: {
            width: 5,
            height: 4,
          },
          items: [
            {
              type: ItemType.OBSTACLE,
              position: {
                x: 2,
                y: 0,
              },
            },
            {
              type: ItemType.OBSTACLE,
              position: {
                x: 0,
                y: 3,
              },
            },
            {
              type: ItemType.OBSTACLE,
              position: {
                x: 3,
                y: 2,
              },
            },
          ],
        },
        roverSection: {
          commands: [],
        },
      },
    })
  );
});

test('the mars rover parser successfully parse a valid input with no obstacles and no commands', () => {
  const input = `Grid
Size 5 4
Commands
`;

  expect(parse(input)).toEqual(
    Success({
      input,
      index: 23,
      value: {
        gridSection: {
          size: {
            width: 5,
            height: 4,
          },
          items: [],
        },
        roverSection: {
          commands: [],
        },
      },
    })
  );
});

test('the mars rover parser fails with missing trailing newline', () => {
  const input = `Grid
Size 5 4
Commands`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 22,
      expected: 'New line at the end of file',
      actual: `
2 | Size 5 4
3 | Commands
   ^
`,
    })
  );
});

test('the mars rover parser fails parsing an input without map definition', () => {
  const input = `Commands
RFF
RF
LFRFFLFFFLL
`;
  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 0,
      expected: 'Grid section begin statement',
      actual: `
1 | Commands
    ^
`,
    })
  );
});

test('the mars rover parser fails parsing an input without commands for the rover', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 53,
      expected: 'Commands section begin statement',
      actual: `
4 | Obstacle 0 3
5 | Obstacle 3 2
6 | 
    ^
`,
    })
  );
});

test('the mars rover parser fails parsing an input without the grid width', () => {
  const input = `Grid
Size  4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 10,
      expected: 'Size width height',
      actual: `
1 | Grid
2 | Size  4
         ^
`,
    })
  );
});

test('the mars rover parser fails parsing an input without the grid height', () => {
  const input = `Grid
Size 5
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 11,
      expected: 'Size width height',
      actual: `
1 | Grid
2 | Size 5
3 | Obstacle 2 0
   ^
`,
    })
  );
});

test('the mars rover parser fails parsing an input without an obstacle x coordinate', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle  3
Obstacle 3 2
`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 35,
      expected: 'Obstacle x y',
      actual: `
2 | Size 5 4
3 | Obstacle 2 0
4 | Obstacle  3
            ^
`,
    })
  );
});

test('the mars rover parser fails parsing an input without an obstacle y coordinate', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 
Obstacle 3 2
`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 35,
      expected: 'Obstacle x y',
      actual: `
2 | Size 5 4
3 | Obstacle 2 0
4 | Obstacle 0 
            ^
`,
    })
  );
});

test('the mars rover parser fails parsing an input with wrong commands for the rover', () => {
  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
LFFRB
FFWRONG!
`;

  expect(parse(input)).toEqual(
    Failure({
      input,
      index: 70,
      expected: 'One of R, L, F, B',
      actual: `
6 | Commands
7 | LFFRB
8 | FFWRONG!
      ^
`,
    })
  );
});
