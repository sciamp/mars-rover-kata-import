# @mars-rover/parser

The `@mars-rover/parser` relies on [`@mars-rover/parser-combinators`](/packages/parser-combinators) to combines its base parsers with its combinators to parse the input file
```
Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
RFF
RF
LFRFFLFFFLL

```
into this AST
```
{
  gridSection: {
    size: {
      width: 5,
      height: 4,
    },
    items: [
      {
        type: ItemType.OBSTACLE,
        position: {
          x: 2,
          y: 0,
        },
      },
      {
        type: ItemType.OBSTACLE,
        position: {
          x: 0,
          y: 3,
        },
      },
      {
        type: ItemType.OBSTACLE,
        position: {
          x: 3,
          y: 2,
        },
      },
    ],
  },
  roverSection: {
    commands: [
      [
        { type: MovementType.ROTATE, direction: Directions.RIGHT },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
      ],
      [
        { type: MovementType.ROTATE, direction: Directions.RIGHT },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
      ],
      [
        { type: MovementType.ROTATE, direction: Directions.LEFT },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
        { type: MovementType.ROTATE, direction: Directions.RIGHT },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
        { type: MovementType.ROTATE, direction: Directions.LEFT },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
        { type: MovementType.MOVE, direction: Directions.FORWARD },
        { type: MovementType.ROTATE, direction: Directions.LEFT },
        { type: MovementType.ROTATE, direction: Directions.LEFT },
      ],
    ],
  },
}
```
## known limitation
The input file must have an empty trailing newline.
