import { point } from '@mars-rover/geometry';
import { ItemType } from '@mars-rover/parser';
import { Item, itemList, fromGridItem } from './items';

test('itemList is a function', () => {
  expect(typeof itemList).toBe('function');
});

test('itemList takes a list of Items and return a ItemMap', () => {
  expect(itemList([]).itemsAtLocation).toBeDefined();
});

test('the itemsAtLocation function returns any items found at the given location', () => {
  const items = itemList([
    { location: point(36, -489), items: [Item.OBSTACLE] },
  ]);

  expect(items.itemsAtLocation(point(36, -489))).toEqual([Item.OBSTACLE]);
});

test('the itemsAtLocation function returns an empty list if the given location is empty', () => {
  const items = itemList([
    { location: point(36, -489), items: [Item.OBSTACLE] },
  ]);

  expect(items.itemsAtLocation(point(42, 42))).toEqual([]);
});

test('the fromGridItem helper takes a GridItem and returns an ItemsAtLocation', () => {
  const itemsAtLocations = fromGridItem({
    type: ItemType.OBSTACLE,
    position: { x: 42, y: 0 },
  });

  expect(itemsAtLocations.location.x).toBe(42);
  expect(itemsAtLocations.location.y).toBe(0);
  expect(itemsAtLocations.items.length).toBe(1);
  expect(itemsAtLocations.items[0]).toBe(Item.OBSTACLE);
});
