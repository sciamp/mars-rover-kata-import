import { point as makePoint, Point } from '@mars-rover/geometry';
import { GridItem, ItemType } from '@mars-rover/parser';

export enum Item {
  UNKNOWN,
  OBSTACLE,
}

export interface ItemMap {
  itemsAtLocation: (location: Point) => Item[];
}

const pointAsString = (point: Point) => `point_${point.x}_${point.y}`;

export interface ItemsAtLocation {
  location: Point;
  items: Item[];
}

const makeLocationItemsMap = (itemsAtLocationList: ItemsAtLocation[]) =>
  itemsAtLocationList.reduce<{
    [location: string]: Item[];
  }>(
    (map, { location, items }) => ({
      ...map,
      [pointAsString(location)]: [
        ...(map[pointAsString(location)] || []),
        ...items,
      ],
    }),
    {}
  );

export const itemList = (itemsAtLocationList: ItemsAtLocation[]): ItemMap => {
  const locationItemsMap = makeLocationItemsMap(itemsAtLocationList);

  return {
    itemsAtLocation: (location: Point) =>
      locationItemsMap[pointAsString(location)] || [],
  };
};

const mapItemTypeToItem = (itemType: ItemType): Item => {
  switch (itemType) {
    case ItemType.OBSTACLE:
      return Item.OBSTACLE;
    default:
      return Item.UNKNOWN;
  }
};

export const fromGridItem = (gridItem: GridItem): ItemsAtLocation => ({
  location: makePoint(gridItem.position.x, gridItem.position.y),
  items: [mapItemTypeToItem(gridItem.type)],
});
