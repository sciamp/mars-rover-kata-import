import { Point, boundingBox, point } from '@mars-rover/geometry';
import { Item } from './items';
import { itemList, ItemsAtLocation } from './items/items';

export interface Map {
  inspect: (point: Point) => Item[] | 'OUT_OF_BOUNDS';
  readonly width: number;
  readonly height: number;
}

interface MakeMapInput {
  width: number;
  height: number;
  items: ItemsAtLocation[];
}

const emptyMap: Map = {
  inspect: () => 'OUT_OF_BOUNDS',
  get width() {
    return 0;
  },
  get height() {
    return 0;
  },
};

export const makeMap = ({ width, height, items }: MakeMapInput): Map => {
  if (width === 0 || height === 0) return emptyMap;

  const grid = boundingBox(
    point(0, 0),
    point(Math.max(width - 1, 0), Math.max(height - 1, 0))
  );
  const itemsMap = itemList(items);

  return {
    inspect: (location: Point) => {
      if (!grid.includes(location)) return 'OUT_OF_BOUNDS';

      return itemsMap.itemsAtLocation(location);
    },
    get width() {
      return width;
    },
    get height() {
      return height;
    },
  };
};
