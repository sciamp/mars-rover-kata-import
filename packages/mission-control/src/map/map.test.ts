import { point } from '@mars-rover/geometry';
import { Item } from './items/items';
import { makeMap } from './map';

test('the makeMap function takes a width, a legnth and a list of items and returns a Map', () => {
  expect(makeMap({ width: 300, height: 100, items: [] }).inspect).toBeDefined();
  expect(makeMap({ width: 300, height: 100, items: [] }).width).toBeDefined();
  expect(makeMap({ width: 300, height: 100, items: [] }).height).toBeDefined();
});

test('the map allows inspecting its width', () => {
  expect(makeMap({ width: 300, height: 100, items: [] }).width).toBe(300);
});

test('the map allows inspecting its height', () => {
  expect(makeMap({ width: 300, height: 100, items: [] }).height).toBe(100);
});

test('running the inspect function against an empty location returns an empty list', () => {
  expect(
    makeMap({
      width: 300,
      height: 100,
      items: [{ location: point(42, 42), items: [Item.OBSTACLE] }],
    }).inspect(point(42, 41))
  ).toEqual([]);
});

test('running the inspect function against a non-empty location returns the list of items at that location', () => {
  expect(
    makeMap({
      width: 300,
      height: 100,
      items: [{ location: point(42, 42), items: [Item.OBSTACLE] }],
    }).inspect(point(42, 42))
  ).toEqual([Item.OBSTACLE]);
});

test('running the inspect function against a location out of map\'s bounds returns "OUT_OF_BOUNDS"', () => {
  expect(
    makeMap({
      width: 300,
      height: 100,
      items: [{ location: point(42, 42), items: [Item.OBSTACLE] }],
    }).inspect(point(700, 42))
  ).toEqual('OUT_OF_BOUNDS');
});

test('running the inspect function against a location whose x coordinate is the grid width returns "OUT_OF_BOUNDS"', () => {
  expect(
    makeMap({
      width: 300,
      height: 100,
      items: [{ location: point(42, 42), items: [Item.OBSTACLE] }],
    }).inspect(point(300, 42))
  ).toEqual('OUT_OF_BOUNDS');
});

test('running the inspect function against a location whose x coordinate is the grid height returns "OUT_OF_BOUNDS"', () => {
  expect(
    makeMap({
      width: 300,
      height: 100,
      items: [{ location: point(42, 42), items: [Item.OBSTACLE] }],
    }).inspect(point(42, 100))
  ).toEqual('OUT_OF_BOUNDS');
});

test('point(0, 0) is not out of bounds if width and height are positive', () => {
  expect(
    makeMap({ width: 1, height: 1, items: [] }).inspect(point(0, 0))
  ).toEqual([]);
});

test('makeMap returns an empty map if width is zero', () => {
  expect(
    makeMap({
      width: 0,
      height: 100,
      items: [{ location: point(0, 42), items: [Item.OBSTACLE] }],
    }).inspect(point(0, 42))
  ).toEqual('OUT_OF_BOUNDS');
});

test('makeMap returns an empty map if height is zero', () => {
  expect(
    makeMap({
      width: 100,
      height: 0,
      items: [{ location: point(41, 0), items: [Item.OBSTACLE] }],
    }).inspect(point(42, 0))
  ).toEqual('OUT_OF_BOUNDS');
});
