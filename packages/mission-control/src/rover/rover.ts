import { Point } from '@mars-rover/geometry';
import { Directions } from '@mars-rover/parser';
import { Compass, Radiants } from '../compass';
import { NavDevice, NavDirection } from '../nav-device';

export interface RoverState {
  currentPosition: Point;
  currentDirection: NavDirection;
  message?: string;
}

export type RoverStateReport<Report> = (state: RoverState) => Report;

export interface RoverActions {
  rotate: (direction: Directions.LEFT | Directions.RIGHT) => void;
  move: (direction: Directions.FORWARD | Directions.BACKWARD) => void;
}

export interface RoverReport<ReportFormat> {
  report: () => ReportFormat;
}

export interface RoverSetupParams<ReportFormat> {
  position: Point;
  direction: NavDirection;
  navDevice: NavDevice;
  compass: Compass;
  stateReporter: RoverStateReport<ReportFormat>;
}

export class Rover<ReportFormat>
  implements RoverActions, RoverReport<ReportFormat> {
  private state: RoverState;

  private compass: Compass;

  private navDevice: NavDevice;

  private stateReporter: RoverStateReport<ReportFormat>;

  constructor(conf: RoverSetupParams<ReportFormat>) {
    this.state = {
      currentPosition: conf.position,
      currentDirection: conf.direction,
      message: undefined,
    };
    this.compass = conf.compass;
    this.navDevice = conf.navDevice;
    this.stateReporter = conf.stateReporter;
  }

  public report(): ReportFormat {
    return this.stateReporter(this.state);
  }

  public rotate(direction: Directions.LEFT | Directions.RIGHT) {
    this.state = {
      ...this.state,
      currentDirection: this.updateDirection(direction),
    };
  }

  public move(direction: Directions.FORWARD | Directions.BACKWARD) {
    this.state = {
      ...this.state,
      ...this.updatePosition(direction),
    };
  }

  private updateDirection(
    rotationDirection: Directions.LEFT | Directions.RIGHT
  ): NavDirection {
    switch (rotationDirection) {
      case Directions.LEFT:
        return this.compass(this.state.currentDirection, Radiants.HALF_PI);
      case Directions.RIGHT:
        return this.compass(
          this.state.currentDirection,
          Radiants.MINUS_HALF_PI
        );
      default:
        return this.state.currentDirection;
    }
  }

  private updatePosition(
    direction: Directions.FORWARD | Directions.BACKWARD
  ): { currentPosition: Point; message?: string } {
    const navigationResult = this.navDevice.navigate(
      this.state.currentPosition,
      this.getNavDirectionFromMovementDirection(direction)
    );

    return {
      currentPosition: navigationResult.currentPosition,
      message: navigationResult.message,
    };
  }

  private getNavDirectionFromMovementDirection(
    direction: Directions
  ): NavDirection {
    switch (direction) {
      case Directions.BACKWARD:
        return this.compass(this.state.currentDirection, Radiants.PI);
      default:
        return this.state.currentDirection;
    }
  }
}

export const configureRover = <ReportFormat>(
  conf: RoverSetupParams<ReportFormat>
): Rover<ReportFormat> => new Rover(conf);
