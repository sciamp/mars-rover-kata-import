import { point } from '@mars-rover/geometry';
import { Directions } from '@mars-rover/parser';
import { Item, makeMap } from '../map';
import { configureNav, NavDirection, pacMan } from '../nav-device';
import { Radiants } from '../compass';
import { RoverState, configureRover } from './rover';

/* _ _ _ _ _ _ _ _ _ _
9 |_|_|_|_|_|_|_|_|_|_|
8 |_|_|_|_|_|_|_|_|_|_|     N
7 |_|_|_|_|_|_|o|_|_|_|     ^
6 |_|_|_|_|_|_|_|_|_|_|     |
5 |_|_|_|_|_|_|_|_|_|_|   W-+-E
4 |_|_|_|_|_|_|_|_|_|_|     |
3 |_|_|_|o|_|o|_|_|_|_|     S
2 |_|_|_|_|o|_|_|_|_|_|
1 |_|_|_|_|_|_|_|_|_|_|
0 |_|_|_|_|_|_|_|_|_|_|
   0 1 2 3 4 5 6 7 8 9
*/
const testMap = makeMap({
  width: 10,
  height: 10,
  items: [
    { location: point(3, 3), items: [Item.OBSTACLE] },
    { location: point(4, 2), items: [Item.OBSTACLE] },
    { location: point(5, 3), items: [Item.OBSTACLE] },
    { location: point(6, 7), items: [Item.OBSTACLE] },
  ],
});

const makeTestCompass = (returnValue: NavDirection, logs?: any[]) => (
  direction: NavDirection,
  amount: Radiants
) => {
  if (logs)
    logs.push({ functionName: 'rotate', params: { direction, amount } });
  return returnValue;
};

test('the Rover relies on its compass to perform left rotations', () => {
  const logs: any[] = [];
  const rover = configureRover({
    position: point(8, 5),
    direction: NavDirection.WEST,
    navDevice: configureNav({ navMap: testMap, navStrategy: pacMan }),
    compass: makeTestCompass(NavDirection.SOUTH, logs),
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}`,
  });

  expect(logs.length).toEqual(0);

  rover.rotate(Directions.LEFT);

  expect(logs.length).toEqual(1);
  expect(logs[0].functionName).toEqual('rotate');
  expect(logs[0].params.direction).toBe(NavDirection.WEST);
  expect(logs[0].params.amount).toBe(Radiants.HALF_PI);
});

test('the Rover relies on its compass to perform right rotations', () => {
  const logs: any[] = [];
  const rover = configureRover({
    position: point(8, 5),
    direction: NavDirection.WEST,
    navDevice: configureNav({ navMap: testMap, navStrategy: pacMan }),
    compass: makeTestCompass(NavDirection.NORTH, logs),
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}`,
  });

  expect(logs.length).toEqual(0);

  rover.rotate(Directions.RIGHT);

  expect(logs.length).toEqual(1);
  expect(logs[0].functionName).toEqual('rotate');
  expect(logs[0].params.direction).toBe(NavDirection.WEST);
  expect(logs[0].params.amount).toBe(Radiants.MINUS_HALF_PI);
});

test('the Rover successfully updates its state after rotating with the value returned by the compass', () => {
  const rover = configureRover({
    position: point(8, 5),
    direction: NavDirection.WEST,
    navDevice: configureNav({ navMap: testMap, navStrategy: pacMan }),
    compass: makeTestCompass(NavDirection.SOUTH),
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}`,
  });

  rover.rotate(Directions.LEFT);

  expect(rover.report()).toEqual(
    `pos: (${point(8, 5).x},${point(8, 5).y}), dir: ${NavDirection.SOUTH}`
  );
});
