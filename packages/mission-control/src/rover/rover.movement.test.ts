import { Point, point } from '@mars-rover/geometry';
import { Directions } from '@mars-rover/parser';
import { NavDevice, NavDirection, NavResult } from '../nav-device';
import { Radiants } from '../compass';
import { RoverState, configureRover } from './rover';

const makeTestCompass = (returnValue: NavDirection, logs?: any[]) => (
  direction: NavDirection,
  amount: Radiants
) => {
  if (logs)
    logs.push({ functionName: 'rotate', params: { direction, amount } });
  return returnValue;
};

const makeTestNavDevice = (
  returnValue: NavResult,
  logs?: any[]
): NavDevice => ({
  navigate: (startingPoint: Point, direction: NavDirection): NavResult => {
    if (logs)
      logs.push({
        functionName: 'navigate',
        params: { startingPoint, direction },
      });
    return returnValue;
  },
});

test('the Rover relies on its navDevice to perform forward movements', () => {
  const logs: any[] = [];
  const rover = configureRover({
    position: point(8, 5),
    direction: NavDirection.WEST,
    navDevice: makeTestNavDevice(
      { currentPosition: point(7, 5), isError: false, message: 'OK' },
      logs
    ),
    compass: makeTestCompass(NavDirection.SOUTH, logs),
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}, message: ${state.message}`,
  });

  expect(logs.length).toEqual(0);

  rover.move(Directions.FORWARD);

  expect(logs.length).toEqual(1);
  expect(logs[0].functionName).toEqual('navigate');
  expect(logs[0].params.startingPoint.x).toBe(8);
  expect(logs[0].params.startingPoint.y).toBe(5);
  expect(logs[0].params.direction).toBe(NavDirection.WEST);
});

test('the Rover relies on its navDevice to perform backward movements', () => {
  const logs: any[] = [];
  const rover = configureRover({
    position: point(8, 5),
    direction: NavDirection.WEST,
    navDevice: makeTestNavDevice(
      { currentPosition: point(9, 5), isError: false, message: 'OK' },
      logs
    ),
    compass: makeTestCompass(NavDirection.EAST, logs),
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}, message: ${state.message}`,
  });

  expect(logs.length).toEqual(0);

  rover.move(Directions.BACKWARD);

  expect(logs.length).toEqual(2);

  expect(logs[0].functionName).toEqual('rotate');
  expect(logs[0].params.direction).toBe(NavDirection.WEST);
  expect(logs[0].params.amount).toBe(Radiants.PI);

  expect(logs[1].functionName).toEqual('navigate');
  expect(logs[1].params.startingPoint.x).toBe(8);
  expect(logs[1].params.startingPoint.y).toBe(5);
  expect(logs[1].params.direction).toBe(NavDirection.EAST);
});

test('the Rover successfully updates its state after moving with the value returned by the navDevice', () => {
  const rover = configureRover({
    position: point(8, 5),
    direction: NavDirection.WEST,
    navDevice: makeTestNavDevice({
      currentPosition: point(9, 5),
      isError: false,
      message: 'OK',
    }),
    compass: makeTestCompass(NavDirection.EAST),
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}, message: ${state.message}`,
  });

  rover.move(Directions.BACKWARD);

  expect(rover.report()).toEqual(
    `pos: (${point(9, 5).x},${point(9, 5).y}), dir: ${
      NavDirection.WEST
    }, message: OK`
  );
});
