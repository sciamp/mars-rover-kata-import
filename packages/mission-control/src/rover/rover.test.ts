import { point } from '@mars-rover/geometry';
import { Item, makeMap } from '../map';
import { configureNav, NavDirection, pacMan } from '../nav-device';
import { rotate } from '../compass';
import { RoverState, Rover, configureRover } from './rover';

/* _ _ _ _ _ _ _ _ _ _
9 |_|_|_|_|_|_|_|_|_|_|
8 |_|_|_|_|_|_|_|_|_|_|     N
7 |_|_|_|_|_|_|o|_|_|_|     ^
6 |_|_|_|_|_|_|_|_|_|_|     |
5 |_|_|_|_|_|_|_|_|_|_|   W-+-E
4 |_|_|_|_|_|_|_|_|_|_|     |
3 |_|_|_|o|_|o|_|_|_|_|     S
2 |_|_|_|_|o|_|_|_|_|_|
1 |_|_|_|_|_|_|_|_|_|_|
0 |_|_|_|_|_|_|_|_|_|_|
   0 1 2 3 4 5 6 7 8 9
*/
const testMap = makeMap({
  width: 10,
  height: 10,
  items: [
    { location: point(3, 3), items: [Item.OBSTACLE] },
    { location: point(4, 2), items: [Item.OBSTACLE] },
    { location: point(5, 3), items: [Item.OBSTACLE] },
    { location: point(6, 7), items: [Item.OBSTACLE] },
  ],
});

test('the configureRover helper takes a Point, a NavDirection, a NavDevice, a Compass and a RoverStateReport and returns a configured Rover', () => {
  const rover = configureRover({
    position: point(0, 0),
    direction: NavDirection.NORTH,
    navDevice: configureNav({ navMap: testMap, navStrategy: pacMan }),
    compass: rotate,
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}`,
  });

  expect(rover).toBeInstanceOf(Rover);
});

test('the Rover reports its state', () => {
  const rover = configureRover({
    position: point(8, 5),
    direction: NavDirection.WEST,
    navDevice: configureNav({ navMap: testMap, navStrategy: pacMan }),
    compass: rotate,
    stateReporter: (state: RoverState) =>
      `pos: (${state.currentPosition.x},${state.currentPosition.y}), dir: ${state.currentDirection}`,
  });

  expect(rover.report()).toEqual(
    `pos: (${point(8, 5).x},${point(8, 5).y}), dir: ${NavDirection.WEST}`
  );
});
