/* eslint-disable no-restricted-syntax */
import { point } from '@mars-rover/geometry';
import {
  GridSection,
  isMovement,
  isRotation,
  RoverCommand,
} from '@mars-rover/parser';
import { rotate } from './compass';
import { makeMap, Map } from './map';
import { fromGridItem } from './map/items/items';
import { configureNav, NavDirection, pacMan } from './nav-device';
import { configureRover, Rover, RoverState } from './rover';

export class MissionControl {
  private rover: Rover<string>;

  private commands: RoverCommand[];

  constructor(grid: GridSection, commands: RoverCommand[]) {
    this.rover = configureRover({
      position: point(0, 0),
      direction: NavDirection.NORTH,
      navDevice: configureNav({
        navMap: MissionControl.mapFromGrid(grid),
        navStrategy: pacMan,
      }),
      compass: rotate,
      stateReporter: MissionControl.stateReporter,
    });
    this.commands = commands;
  }

  public *run(): Generator<string, void, void> {
    for (const command of this.commands) {
      for (const action of command) {
        if (isRotation(action)) {
          this.rover.rotate(action.direction);
        } else if (isMovement(action)) {
          this.rover.move(action.direction);
        }
      }
      yield this.rover.report();
    }
  }

  private static mapFromGrid(grid: GridSection): Map {
    return makeMap({
      width: grid.size.width,
      height: grid.size.height,
      items: grid.items.map(fromGridItem),
    });
  }

  private static stateReporter(state: RoverState): string {
    const reportStringSeparator = ':';
    const positionReport = [
      state.currentPosition.x,
      state.currentPosition.y,
    ].join(reportStringSeparator);
    const directionReport = MissionControl.navDirectionAsString(
      state.currentDirection
    );
    const vectorReport = [positionReport, directionReport].join(
      reportStringSeparator
    );

    if (state.message && state.message === 'DESTINATION IS NOT EMPTY')
      return ['O', vectorReport].join(reportStringSeparator);

    return vectorReport;
  }

  private static navDirectionAsString(direction: NavDirection): string {
    switch (direction) {
      case NavDirection.NORTH:
        return 'N';
      case NavDirection.SOUTH:
        return 'S';
      case NavDirection.WEST:
        return 'W';
      case NavDirection.EAST:
        return 'E';
      default:
        return '';
    }
  }
}
