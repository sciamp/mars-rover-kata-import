import { parse } from '@mars-rover/parser';
import { MissionControl } from './mission-control';

test('Run mission', () => {
  expect.assertions(4);

  const input = `Grid
Size 5 4
Obstacle 2 0
Obstacle 0 3
Obstacle 3 2
Commands
RFF
RF
LFRFFLFFFLL
`;
  parse(input).match(
    ({
      value: {
        gridSection,
        roverSection: { commands },
      },
    }) => {
      const missionControl = new MissionControl(gridSection, commands);

      const logs = [];
      // eslint-disable-next-line no-restricted-syntax
      for (const report of missionControl.run()) {
        logs.push(report);
      }

      expect(logs.length).toBe(3);
      expect(logs[0]).toBe('O:1:0:E');
      expect(logs[1]).toBe('1:3:S');
      expect(logs[2]).toBe('0:1:W');
    },
    () => undefined
  );
});
