import { NavDirection } from '../nav-device/nav-device';
import { Radiants, rotate } from './compass';

test('the rotate function rotates the north direction by +90 degrees', () => {
  expect(rotate(NavDirection.NORTH, Radiants.HALF_PI)).toEqual(
    NavDirection.WEST
  );
});

test('the rotate function rotates the west direction by +90 degrees', () => {
  expect(rotate(NavDirection.WEST, Radiants.HALF_PI)).toEqual(
    NavDirection.SOUTH
  );
});

test('the rotate function rotates the south direction by +90 degrees', () => {
  expect(rotate(NavDirection.SOUTH, Radiants.HALF_PI)).toEqual(
    NavDirection.EAST
  );
});

test('the rotate function rotates the east direction by +90 degrees', () => {
  expect(rotate(NavDirection.EAST, Radiants.HALF_PI)).toEqual(
    NavDirection.NORTH
  );
});

test('the rotate function rotates the north direction by -90 degrees', () => {
  expect(rotate(NavDirection.NORTH, Radiants.MINUS_HALF_PI)).toEqual(
    NavDirection.EAST
  );
});

test('the rotate function rotates the east direction by -90 degrees', () => {
  expect(rotate(NavDirection.EAST, Radiants.MINUS_HALF_PI)).toEqual(
    NavDirection.SOUTH
  );
});

test('the rotate function rotates the south direction by -90 degrees', () => {
  expect(rotate(NavDirection.SOUTH, Radiants.MINUS_HALF_PI)).toEqual(
    NavDirection.WEST
  );
});

test('the rotate function rotates the west direction by -90 degrees', () => {
  expect(rotate(NavDirection.WEST, Radiants.MINUS_HALF_PI)).toEqual(
    NavDirection.NORTH
  );
});

test('the rotate function rotates the north direction by 180 degrees', () => {
  expect(rotate(NavDirection.NORTH, Radiants.PI)).toEqual(NavDirection.SOUTH);
});

test('the rotate function rotates the south direction by 180 degrees', () => {
  expect(rotate(NavDirection.SOUTH, Radiants.PI)).toEqual(NavDirection.NORTH);
});

test('the rotate function rotates the west direction by 180 degrees', () => {
  expect(rotate(NavDirection.WEST, Radiants.PI)).toEqual(NavDirection.EAST);
});

test('the rotate function rotates the east direction by 180 degrees', () => {
  expect(rotate(NavDirection.EAST, Radiants.PI)).toEqual(NavDirection.WEST);
});
