import { NavDirection } from '../nav-device/nav-device';

export enum Radiants {
  HALF_PI,
  PI,
  MINUS_HALF_PI,
}

export type Compass = (
  currentDirection: NavDirection,
  amount: Radiants
) => NavDirection;

const rotationCycles = {
  [Radiants.HALF_PI]: {
    [NavDirection.NORTH]: NavDirection.WEST,
    [NavDirection.WEST]: NavDirection.SOUTH,
    [NavDirection.SOUTH]: NavDirection.EAST,
    [NavDirection.EAST]: NavDirection.NORTH,
  },
  [Radiants.MINUS_HALF_PI]: {
    [NavDirection.NORTH]: NavDirection.EAST,
    [NavDirection.EAST]: NavDirection.SOUTH,
    [NavDirection.SOUTH]: NavDirection.WEST,
    [NavDirection.WEST]: NavDirection.NORTH,
  },
  [Radiants.PI]: {
    [NavDirection.NORTH]: NavDirection.SOUTH,
    [NavDirection.SOUTH]: NavDirection.NORTH,
    [NavDirection.WEST]: NavDirection.EAST,
    [NavDirection.EAST]: NavDirection.WEST,
  },
};

export const rotate = (direction: NavDirection, amount: Radiants) =>
  rotationCycles[amount][direction];
