import { point } from '@mars-rover/geometry';
import { makeMap } from '../../map';
import { Item } from '../../map/items';
import { NavDirection, NavResult } from '../nav-device';
import { pacMan } from './pac-man-navigation';

/* _ _ _ _ _ _ _ _ _ _
9 |_|_|_|_|_|_|_|_|_|_|
8 |_|_|_|_|_|_|_|_|_|_|     N
7 |_|_|_|_|_|_|o|_|_|_|     ^
6 |_|_|_|_|_|_|_|_|_|_|     |
5 |_|_|_|_|_|_|_|_|_|_|   W-+-E
4 |_|_|_|_|_|_|_|_|_|_|     |
3 |_|_|_|o|_|o|_|_|_|_|     S
2 |_|_|_|_|o|_|_|_|_|_|
1 |_|_|_|_|_|_|_|_|_|_|
0 |_|_|_|_|_|_|_|_|_|_|
   0 1 2 3 4 5 6 7 8 9
*/
const testMap = makeMap({
  width: 10,
  height: 10,
  items: [
    { location: point(3, 3), items: [Item.OBSTACLE] },
    { location: point(4, 2), items: [Item.OBSTACLE] },
    { location: point(5, 3), items: [Item.OBSTACLE] },
    { location: point(6, 7), items: [Item.OBSTACLE] },
  ],
});

const navigate = pacMan(testMap);

test('pacman navigation strategy successfully navigates to a northern empty location', () => {
  const result: NavResult = navigate(point(7, 4), NavDirection.NORTH);

  expect(result.currentPosition.x).toBe(7);
  expect(result.currentPosition.y).toBe(5);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});

test('pacman navigation strategy successfully navigates to a southern empty location', () => {
  const result: NavResult = navigate(point(7, 4), NavDirection.SOUTH);

  expect(result.currentPosition.x).toBe(7);
  expect(result.currentPosition.y).toBe(3);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});

test('pacman navigation strategy successfully navigates to a western empty location', () => {
  const result: NavResult = navigate(point(7, 4), NavDirection.WEST);

  expect(result.currentPosition.x).toBe(6);
  expect(result.currentPosition.y).toBe(4);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});

test('pacman navigation strategy successfully navigates to a eastern empty location', () => {
  const result: NavResult = navigate(point(7, 4), NavDirection.EAST);

  expect(result.currentPosition.x).toBe(8);
  expect(result.currentPosition.y).toBe(4);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});

test('pacman navigation strategy fails navigating to a northern location with and obstacle', () => {
  const result: NavResult = navigate(point(6, 6), NavDirection.NORTH);

  expect(result.currentPosition.x).toBe(6);
  expect(result.currentPosition.y).toBe(6);
  expect(result.isError).toBe(true);
  expect(result.message).toBe('DESTINATION IS NOT EMPTY');
});

test('pacman navigation strategy fails navigating to a southern location with and obstacle', () => {
  const result: NavResult = navigate(point(6, 8), NavDirection.SOUTH);

  expect(result.currentPosition.x).toBe(6);
  expect(result.currentPosition.y).toBe(8);
  expect(result.isError).toBe(true);
  expect(result.message).toBe('DESTINATION IS NOT EMPTY');
});

test('pacman navigation strategy fails navigating to a western location with and obstacle', () => {
  const result: NavResult = navigate(point(7, 7), NavDirection.WEST);

  expect(result.currentPosition.x).toBe(7);
  expect(result.currentPosition.y).toBe(7);
  expect(result.isError).toBe(true);
  expect(result.message).toBe('DESTINATION IS NOT EMPTY');
});

test('pacman navigation strategy fails navigating to a eastern location with and obstacle', () => {
  const result: NavResult = navigate(point(5, 7), NavDirection.EAST);

  expect(result.currentPosition.x).toBe(5);
  expect(result.currentPosition.y).toBe(7);
  expect(result.isError).toBe(true);
  expect(result.message).toBe('DESTINATION IS NOT EMPTY');
});

test('pacman navigation strategy successfully navigates beyond the northern border', () => {
  const result: NavResult = navigate(point(4, 9), NavDirection.NORTH);

  expect(result.currentPosition.x).toBe(4);
  expect(result.currentPosition.y).toBe(0);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});

test('pacman navigation strategy successfully navigates beyond the southern border', () => {
  const result: NavResult = navigate(point(3, 0), NavDirection.SOUTH);

  expect(result.currentPosition.x).toBe(3);
  expect(result.currentPosition.y).toBe(9);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});

test('pacman navigation strategy successfully navigates beyond the western border', () => {
  const result: NavResult = navigate(point(0, 2), NavDirection.WEST);

  expect(result.currentPosition.x).toBe(9);
  expect(result.currentPosition.y).toBe(2);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});

test('pacman navigation strategy successfully navigates beyond the eastern border', () => {
  const result: NavResult = navigate(point(9, 3), NavDirection.EAST);

  expect(result.currentPosition.x).toBe(0);
  expect(result.currentPosition.y).toBe(3);
  expect(result.isError).toBe(false);
  expect(result.message).toBeUndefined();
});
