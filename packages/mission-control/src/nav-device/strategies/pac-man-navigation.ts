import { point, Point } from '@mars-rover/geometry';
import { Map } from '../../map';
import { NavDirection } from '../nav-device';
import { NavStrategy } from '../nav-strategy';

const positiveModule = (amount: number, module: number) => {
  const amountInModule = amount % module;

  if (amountInModule >= 0) return amountInModule;

  return module - 1;
};

const getDestinationPoint = (
  startingPoint: Point,
  direction: NavDirection,
  map: Map
) => {
  switch (direction) {
    case NavDirection.NORTH:
      return point(
        startingPoint.x,
        positiveModule(startingPoint.y + 1, map.height)
      );
    case NavDirection.SOUTH:
      return point(
        startingPoint.x,
        positiveModule(startingPoint.y - 1, map.height)
      );
    case NavDirection.WEST:
      return point(
        positiveModule(startingPoint.x - 1, map.width),
        startingPoint.y
      );
    case NavDirection.EAST:
      return point(
        positiveModule(startingPoint.x + 1, map.width),
        startingPoint.y
      );
    default:
      return startingPoint;
  }
};

export const pacMan: NavStrategy = (map: Map) => (
  startingPoint: Point,
  direction: NavDirection
) => {
  const destinationPoint = getDestinationPoint(startingPoint, direction, map);

  if (map.inspect(destinationPoint).length === 0)
    return {
      currentPosition: destinationPoint,
      isError: false,
    };

  return {
    currentPosition: startingPoint,
    isError: true,
    message: 'DESTINATION IS NOT EMPTY',
  };
};
