import { Point } from '@mars-rover/geometry';
import { Map } from '../map';
import { NavStrategy } from './nav-strategy';

export enum NavDirection {
  NORTH,
  SOUTH,
  WEST,
  EAST,
}

export interface NavResult {
  currentPosition: Point;
  isError: boolean;
  message?: string;
}

export interface NavDevice {
  navigate: (startingPoint: Point, direction: NavDirection) => NavResult;
}

interface ConfigureNavInput {
  navMap: Map;
  navStrategy: NavStrategy;
}

export const configureNav = ({
  navMap,
  navStrategy,
}: ConfigureNavInput): NavDevice => {
  const strategy = navStrategy(navMap);

  return {
    navigate: (startingPoint: Point, direction: NavDirection): NavResult =>
      strategy(startingPoint, direction),
  };
};
