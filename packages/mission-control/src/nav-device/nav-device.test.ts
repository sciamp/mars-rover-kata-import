import { point, Point } from '@mars-rover/geometry';
import { NavResult, NavDirection, configureNav } from './nav-device';
import { NavStrategy } from './nav-strategy';
import { makeMap, Map } from '../map';

const makeStrategy = (navResult: NavResult, callLogs?: any[]): NavStrategy => (
  map: Map
) => (startingPoint: Point, direction: NavDirection) => {
  callLogs?.push({ map, startingPoint, direction });
  return navResult;
};

const testMap = makeMap({ width: 10, height: 10, items: [] });

test('the configureNav function takes a Map and a NavStrategy and returns a NavDevice', () => {
  const strategy = makeStrategy({
    currentPosition: point(0, 0),
    isError: true,
    message: 'foo bar',
  });

  expect(
    configureNav({ navMap: testMap, navStrategy: strategy }).navigate
  ).toBeDefined();
});

test('a NavDevice calls the NavStrategy to navigate', () => {
  const callLogs: any[] = [];
  const strategy = makeStrategy(
    {
      currentPosition: point(2, 4),
      isError: false,
    },
    callLogs
  );

  expect(callLogs.length).toBe(0);

  configureNav({ navMap: testMap, navStrategy: strategy }).navigate(
    point(2, 3),
    NavDirection.NORTH
  );

  expect(callLogs.length).toBe(1);
});

test('the result of a navigation performed by a NavDevice is the one returned from its configured NavStrategy', () => {
  const strategy = makeStrategy({
    currentPosition: point(42, 0),
    isError: true,
    message: 'foo bar',
  });

  const { currentPosition, isError, message } = configureNav({
    navMap: testMap,
    navStrategy: strategy,
  }).navigate(point(2, 3), NavDirection.NORTH);

  expect(currentPosition.x).toBe(42);
  expect(currentPosition.y).toBe(0);
  expect(isError).toBe(true);
  expect(message).toBe('foo bar');
});
