import { Point } from '@mars-rover/geometry';
import { Map } from '../map';
import { NavDirection, NavResult } from './nav-device';

export type NavStrategy = (
  map: Map
) => (startingPoint: Point, direction: NavDirection) => NavResult;
