# @mars-rover/mission-control
The `@mars-rover/mission-control` packages is responsible of building and operating all the entities needed to run a mission of the mars rover.

## components

### rover
The `Rover` is made up of a `Compass` and a `NavDevice` and it's able to perform left/right rotations and forward/backward movements and updates its internal state accordingly. The rover is also equipped with a `stateReporter` tool to give updates to mission control through the report action.
```ts
enum NavDirection {
  NORTH,
  SOUTH,
  WEST,
  EAST,
}

enum Directions {
  FORWARD,
  BACKWARD,
  RIGHT,
  LEFT,
}

interface RoverState {
  currentPosition: Point;
  currentDirection: NavDirection;
  message?: string;
}

type RoverStateReport<Report> = (state: RoverState) => Report;

interface RoverActions {
  rotate: (direction: Directions.LEFT | Directions.RIGHT) => void;
  move: (direction: Directions.FORWARD | Directions.BACKWARD) => void;
}

interface RoverReport<ReportFormat> {
  report: () => ReportFormat;
}

interface RoverSetupParams<ReportFormat> {
  position: Point;
  direction: NavDirection;
  navDevice: NavDevice;
  compass: Compass;
  stateReporter: RoverStateReport<ReportFormat>;
}

class Rover<ReportFormat> implements RoverActions, RoverReport<ReportFormat>
```

### compass
The compass helps the rover to understand what direction is facing after a rotation, it provides a `rotate: Compass` function for doing this:
```ts
enum Radiants {
  HALF_PI,
  PI,
  MINUS_HALF_PI,
}

enum NavDirection {
  NORTH,
  SOUTH,
  WEST,
  EAST,
}

type Compass = (currentDirection: NavDirection, amount: Radiants) => NavDirection
```

### nav device
The nav device knows the topology of the rover surroundings and helps it moving around. To do so it needs a map:
```ts
interface Map {
  inspect: (point: Point) => Item[] | 'OUT_OF_BOUNDS';
  readonly width: number;
  readonly height: number;
}
```
and a navigation strategy that is the real core of the nav sytem:
```ts
type NavStrategy = (
  map: Map
) => (startingPoint: Point, direction: NavDirection) => NavResult;
```
Maps and navigation strategies can be configured during the nav device setup:
```ts
const configureNav = ({
  navMap,
  navStrategy,
}: ConfigureNavInput): NavDevice => {
  const strategy = navStrategy(navMap);

  return {
    navigate: (startingPoint: Point, direction: NavDirection): NavResult =>
      strategy(startingPoint, direction),
  };
};
```

### map
In order to save precious bytes inside the rover memory the map is just a bounding box and a list of non-empty locations (of course this is good when the number of empty locations it's bigger than the number of non-empty ones, but I figured it's Mars not Mauerpark on a sunny sunday afternoon!):
```ts
const makeMap = ({ width, height, items }: MakeMapInput): Map => {
  if (width === 0 || height === 0) return emptyMap;

  const grid = boundingBox(
    point(0, 0),
    point(Math.max(width - 1, 0), Math.max(height - 1, 0))
  );
  const itemsMap = itemList(items);

  return {
    inspect: (location: Point) => {
      if (!grid.includes(location)) return 'OUT_OF_BOUNDS';

      return itemsMap.itemsAtLocation(location);
    },
    get width() {
      return width;
    },
    get height() {
      return height;
    },
  };
};
```

## mission control
The `MissionControl` class wrap everything up
```ts
this.rover = configureRover({
  position: point(0, 0),
  direction: NavDirection.NORTH,
  navDevice: configureNav({
    navMap: MissionControl.mapFromGrid(grid),
    navStrategy: pacMan,
  }),
  compass: rotate,
  stateReporter: MissionControl.stateReporter,
});
```
and given a list of commands for the rover returns an interator that reports the current rover state after every command execution:
```ts
public *run(): Generator<string, void, void> {
  for (const command of this.commands) {
    for (const action of command) {
      if (isRotation(action)) {
        this.rover.rotate(action.direction);
      } else if (isMovement(action)) {
        this.rover.move(action.direction);
      }
    }
    yield this.rover.report();
  }
}
```

### possible improvements
`MissionControl` could benefit a lot making testing easier by allowing the user to specify in the constructor input all the components to wrap. This would also allow us to easily switch rover, compass or navDevice implementations. 
