import { bind } from './bind';
import { many } from './many';
import { Parser } from './parser';
import { Context, ParserSuccess, Success } from './parser-result';
import { stringParser } from './string';
import { then } from './then';

test('the map higher order parser transform a parser successful result', () => {
  const fooParser = stringParser('foo');
  const mappedFooParser = (context: Context) =>
    fooParser(context).map(({ value, ...rest }: ParserSuccess<string>) => ({
      ...rest,
      value: `TOKEN_${value.toUpperCase()}`,
    }));

  expect(mappedFooParser({ input: 'foo bar', index: 0 })).toEqual(
    Success({
      input: 'foo bar',
      index: 3,
      value: 'TOKEN_FOO',
    })
  );
});

test('the map higher order parser transforms the sequence parser combinator result', () => {
  const barParser = (fooValue: string): Parser<string> => (context: Context) =>
    stringParser('bar')(context).map(
      ({ value: barValue, ...rest }: ParserSuccess<string>) => ({
        ...rest,
        value: `${fooValue.toUpperCase()}(${barValue})`,
      })
    );

  const parser = bind(stringParser('foo'), (fooValue: string) =>
    then(stringParser(' '), barParser(fooValue))
  );

  expect(parser({ input: 'foo bar', index: 0 })).toEqual(
    Success({
      input: 'foo bar',
      index: 7,
      value: 'FOO(bar)',
    })
  );
});

test('the map higher order parser transforms the many parser combinator result', () => {
  const parser = (context: Context) =>
    many(stringParser('foobar'))(context).map(
      (result: ParserSuccess<string[]>) => ({
        ...result,
        value: result.value.join('! '),
      })
    );

  expect(
    parser({
      input: 'foobarfoobarfoobarfoobarfoobar',
      index: 0,
    })
  ).toEqual(
    Success({
      input: 'foobarfoobarfoobarfoobarfoobar',
      index: 30,
      value: 'foobar! foobar! foobar! foobar! foobar',
    })
  );
});
