import {
  nonNegativeIntegerParser,
  NON_NEGATIVE_INTEGER,
} from './non-negative-integer';
import { Failure, Success } from './parser-result';

['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].forEach(inputDigit => {
  test(`nonNegativeIntegerParser parser returns success when input is "${inputDigit}"`, () => {
    expect(nonNegativeIntegerParser({ input: inputDigit, index: 0 })).toEqual(
      Success({
        input: inputDigit,
        index: 1,
        value: Number(inputDigit),
      })
    );
  });
});

['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].forEach(inputDigit => {
  test(`nonNegativeIntegerParser parser returns success when input is "${inputDigit}" with non-zero index`, () => {
    expect(
      nonNegativeIntegerParser({ input: `foo ${inputDigit}`, index: 4 })
    ).toEqual(
      Success({
        input: `foo ${inputDigit}`,
        index: 5,
        value: Number(inputDigit),
      })
    );
  });
});

test('nonNegativeIntegerParser parser return failure when input is not a digit', () => {
  expect(nonNegativeIntegerParser({ input: 'foo', index: 0 })).toEqual(
    Failure({
      input: 'foo',
      index: 0,
      expected: NON_NEGATIVE_INTEGER,
      actual: 'foo',
    })
  );
});

test('nonNegativeIntegerParser parser return failure when input is not a digit with non-zero index', () => {
  expect(nonNegativeIntegerParser({ input: '123 foo', index: 4 })).toEqual(
    Failure({
      input: '123 foo',
      index: 4,
      expected: NON_NEGATIVE_INTEGER,
      actual: 'foo',
    })
  );
});

test('nonNegativeIntegerParser parser returns success when input is "2 bar"', () => {
  expect(nonNegativeIntegerParser({ input: '2 bar', index: 0 })).toEqual(
    Success({
      input: '2 bar',
      index: 1,
      value: 2,
    })
  );
});

test('nonNegativeIntegerParser parser returns success when input is "2 bar" with non-zero index', () => {
  expect(nonNegativeIntegerParser({ input: 'foo 2 bar', index: 4 })).toEqual(
    Success({
      input: 'foo 2 bar',
      index: 5,
      value: 2,
    })
  );
});

test('nonNegativeIntegerParser parser return success when input is a sequence of digit not starting with zero', () => {
  expect(nonNegativeIntegerParser({ input: '3457', index: 0 })).toEqual(
    Success({
      input: '3457',
      index: 4,
      value: 3457,
    })
  );
});

test('nonNegativeIntegerParser parser return success when input is a sequence of digit not starting with zero with non-zero index', () => {
  expect(nonNegativeIntegerParser({ input: 'foo 3457', index: 4 })).toEqual(
    Success({
      input: 'foo 3457',
      index: 8,
      value: 3457,
    })
  );
});

test('nonNegativeIntegerParser parser returns failure when input is a sequence of digits starting with zero', () => {
  expect(nonNegativeIntegerParser({ input: '0123', index: 0 })).toEqual(
    Failure({
      input: '0123',
      index: 0,
      expected: NON_NEGATIVE_INTEGER,
      actual: '0123',
    })
  );
});

test('nonNegativeIntegerParser parser returns failure when input is a sequence of digits starting with zero with non-zero index', () => {
  expect(nonNegativeIntegerParser({ input: 'foo 0123', index: 4 })).toEqual(
    Failure({
      input: 'foo 0123',
      index: 4,
      expected: NON_NEGATIVE_INTEGER,
      actual: '0123',
    })
  );
});

test('nonNegativeIntegerParser parser return success when input is "0 "', () => {
  expect(nonNegativeIntegerParser({ input: '0 ', index: 0 })).toEqual(
    Success({
      input: '0 ',
      index: 1,
      value: 0,
    })
  );
});

test('nonNegativeIntegerParser parser return success when input is "0 " with non-zero index', () => {
  expect(nonNegativeIntegerParser({ input: 'foo 0 ', index: 4 })).toEqual(
    Success({
      input: 'foo 0 ',
      index: 5,
      value: 0,
    })
  );
});
