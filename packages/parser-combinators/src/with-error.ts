import { Parser } from './parser';
import { Context, Failure, ParserError } from './parser-result';

const withError = <T>(
  parser: Parser<T>,
  transform: (error: ParserError) => ParserError
): Parser<T> => (context: Context) =>
  parser(context).or(error => Failure(transform(error)));

export { withError };
