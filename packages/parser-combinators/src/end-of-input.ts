import { Parser } from './parser';
import { Context, Failure, ParserResult, Success } from './parser-result';

const END_OF_INPUT = 'END_OF_INPUT';

const endOfInputParser: Parser<string> = ({
  input,
  index,
}: Context): ParserResult<string> => {
  const inputToConsume = input.slice(index);

  if (inputToConsume.length === 0)
    return Success({ input, index, value: END_OF_INPUT });

  return Failure({
    input,
    index,
    expected: END_OF_INPUT,
    actual: inputToConsume,
  });
};

export { END_OF_INPUT, endOfInputParser };
