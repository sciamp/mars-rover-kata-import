import { Context, ParserResult } from './parser-result';

type Parser<T> = (context: Context) => ParserResult<T>;

export { Parser };
