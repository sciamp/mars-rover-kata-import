import { Parser } from './parser';
import { Context, ParserResult, ParserSuccess } from './parser-result';

const bind = <T, S>(
  parser: Parser<T>,
  f: (input: T) => Parser<S>
): Parser<S> => (context: Context) => {
  const result: ParserResult<T> = parser(context);

  return result.flatMap(({ value, ...nextContext }: ParserSuccess<T>) =>
    f(value)(nextContext)
  );
};

export { bind };
