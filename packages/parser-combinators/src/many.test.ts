import { many } from './many';
import { Success } from './parser-result';
import { stringParser } from './string';

test('the many parser combinator successfully parse a stringParser one time', () => {
  const helloParser = stringParser('hello!');
  const manyParser = many(helloParser);
  expect(manyParser({ input: 'hello!', index: 0 })).toEqual(
    Success({
      input: 'hello!',
      index: 6,
      value: ['hello!'],
    })
  );
});

test('the many parser combinator successfully parse a stringParser repeated two times', () => {
  const helloParser = stringParser('hello!');
  const manyParser = many(helloParser);
  expect(manyParser({ input: 'hello!hello!', index: 0 })).toEqual(
    Success({
      input: 'hello!hello!',
      index: 12,
      value: ['hello!', 'hello!'],
    })
  );
});

test('the many parser combinator successfully parse a stringParser three times followed by something else', () => {
  const helloParser = stringParser('hello!');
  const manyParser = many(helloParser);
  expect(
    manyParser({
      input: 'hello!hello!hello! foo bar',
      index: 0,
    })
  ).toEqual(
    Success({
      input: 'hello!hello!hello! foo bar',
      index: 18,
      value: ['hello!', 'hello!', 'hello!'],
    })
  );
});

test('the many parser combinator successfully parse a stringParser 2000 times followed by something else', () => {
  const expectedValues = Array.from({ length: 2000 }, () => 'hello!');
  const input = expectedValues.join('');

  expect(expectedValues.length).toBe(2000);
  const uniqueExpectedValues = Array.from(new Set(expectedValues));
  expect(uniqueExpectedValues.length).toBe(1);
  expect(uniqueExpectedValues[0]).toBe('hello!');

  const helloParser = stringParser('hello!');
  const manyParser = many(helloParser);
  expect(
    manyParser({
      input: `${input} foo bar`,
      index: 0,
    })
  ).toEqual(
    Success({
      input: `${input} foo bar`,
      index: 12000,
      value: expectedValues,
    })
  );
});

test('the many parser combinator succeed with empty result if there are no matches', () => {
  const helloParser = stringParser('hello!');
  const manyParser = many(helloParser);
  expect(manyParser({ input: 'bar!bar!bar!', index: 0 })).toEqual(
    Success({
      input: 'bar!bar!bar!',
      index: 0,
      value: [],
    })
  );
});
