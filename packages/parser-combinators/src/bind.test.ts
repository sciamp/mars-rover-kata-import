import {
  nonNegativeIntegerParser,
  NON_NEGATIVE_INTEGER,
} from './non-negative-integer';
import { Context, Failure, Success } from './parser-result';
import { stringParser } from './string';
import { bind } from './bind';
import { Parser } from './parser';

test('the bind combinator takes a parser and a function and return a new parser', () => {
  const boundParser = (
    previousValue: string
  ): Parser<{ [key: string]: number }> => (context: Context) =>
    nonNegativeIntegerParser(context).map(({ value, ...nextContext }) => ({
      ...nextContext,
      value: { [previousValue]: value },
    }));

  const parser: Parser<{ [key: string]: number }> = bind(
    stringParser('foo'),
    boundParser
  );

  expect(parser({ input: 'foo42', index: 0 })).toEqual(
    Success({
      input: 'foo42',
      index: 5,
      value: { foo: 42 },
    })
  );
});

test('the bind combinator parser fails if the given parser fails', () => {
  const boundParser = (
    previousValue: string
  ): Parser<{ [key: string]: number }> => (context: Context) =>
    nonNegativeIntegerParser(context).map(({ value, ...nextContext }) => ({
      ...nextContext,
      value: { [previousValue]: value },
    }));

  const parser: Parser<{ [key: string]: number }> = bind(
    stringParser('foo'),
    boundParser
  );

  expect(parser({ input: 'bar42', index: 0 })).toEqual(
    Failure({
      input: 'bar42',
      index: 0,
      expected: 'foo',
      actual: 'bar42',
    })
  );
});

test('the bind combinator parser fails if the bound parser fails', () => {
  const boundParser = (
    previousValue: string
  ): Parser<{ [key: string]: number }> => (context: Context) =>
    nonNegativeIntegerParser(context).map(({ value, ...nextContext }) => ({
      ...nextContext,
      value: { [previousValue]: value },
    }));

  const parser: Parser<{ [key: string]: number }> = bind(
    stringParser('foo'),
    boundParser
  );

  expect(parser({ input: 'foo042', index: 0 })).toEqual(
    Failure({
      input: 'foo042',
      index: 3,
      expected: NON_NEGATIVE_INTEGER,
      actual: '042',
    })
  );
});
