import { Context } from './context';

interface ParserSuccess<T> extends Context {
  value: T;
}

export { ParserSuccess };
