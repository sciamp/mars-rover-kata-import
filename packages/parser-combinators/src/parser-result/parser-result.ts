import { ERR, OK, Result } from '@mars-rover/result';
import { ParserError } from './parser-error';
import { ParserSuccess } from './parser-success';

type ParserResult<T> = Result<ParserSuccess<T>, ParserError>;

const Success = <T>(value: ParserSuccess<T>): ParserResult<T> =>
  OK<ParserSuccess<T>, ParserError>(value);

const Failure = <T>(error: ParserError): ParserResult<T> =>
  ERR<ParserSuccess<T>, ParserError>(error);

export { ParserResult, Success, Failure };
