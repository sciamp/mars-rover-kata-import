import { Failure, ParserResult, Success } from './parser-result';

test('the Success helper builds an OkResult', () => {
  const result: ParserResult<number> = Success({
    input: '42',
    index: 2,
    value: 42,
  });

  expect(result.value).toEqual({
    input: '42',
    index: 2,
    value: 42,
  });
  expect(result.error).toBeUndefined();
});

test('the Failure helper builds a ErrResult', () => {
  const result: ParserResult<string> = Failure({
    input: 'bar',
    index: 0,
    expected: 'foo',
    actual: 'bar',
  });

  expect(result.error).toEqual({
    input: 'bar',
    index: 0,
    expected: 'foo',
    actual: 'bar',
  });
  expect(result.value).toBeUndefined();
});
