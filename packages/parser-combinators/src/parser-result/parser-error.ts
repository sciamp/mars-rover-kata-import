import { Context } from './context';

interface ParserError extends Context {
  expected: string;
  actual: string;
}

export { ParserError };
