import { Parser } from './parser';
import { Context, Failure, ParserResult, Success } from './parser-result';

const stringParser = (match: string): Parser<string> => ({
  input,
  index,
}: Context): ParserResult<string> => {
  const inputToConsume = input.slice(index);

  if (inputToConsume.startsWith(match))
    return Success({ input, index: index + match.length, value: match });

  return Failure({ input, index, expected: match, actual: inputToConsume });
};

export { stringParser };
