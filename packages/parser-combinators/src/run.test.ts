import { stringParser } from './string';
import { run } from './run';

test('run takes a parser and an input string and run the given parser against the given input starting from the beginning', () => {
  const parser = stringParser('foo');
  const input = 'foobar!';

  expect(run(parser)(input)).toEqual(parser({ input, index: 0 }));
});
