import { Failure, Success } from './parser-result';
import { stringParser } from './string';
import { withError } from './with-error';

test('the withError combinator takes a parser and does nothing when parsing is successful', () => {
  const parser = stringParser('foo');

  const parserWithError = withError(parser, error => ({
    ...error,
    expected: 'ASD!',
  }));

  expect(parserWithError({ input: 'foo bar', index: 0 })).toEqual(
    Success({
      input: 'foo bar',
      index: 3,
      value: 'foo',
    })
  );
});

test('the withError combinator takes a parser apply the given transformation to the error', () => {
  const parser = stringParser('foo');

  const parserWithError = withError(parser, error => ({
    ...error,
    expected: 'ASD!',
  }));

  expect(parserWithError({ input: 'fo bar', index: 0 })).toEqual(
    Failure({
      input: 'fo bar',
      index: 0,
      expected: 'ASD!',
      actual: 'fo bar',
    })
  );
});
