import { endOfInputParser, END_OF_INPUT } from './end-of-input';
import { Failure, Success } from './parser-result';

test('the endOfInputParser parser successfully parses the end of input', () => {
  expect(endOfInputParser({ input: '', index: 0 })).toEqual(
    Success({
      input: '',
      index: 0,
      value: END_OF_INPUT,
    })
  );
});

test('the endOfInputParser parser successfully parses the end of input with non-zero index', () => {
  expect(endOfInputParser({ input: 'foo bar', index: 7 })).toEqual(
    Success({
      input: 'foo bar',
      index: 7,
      value: END_OF_INPUT,
    })
  );
});

test('the endOfInputParser parser fails if index is not end of input', () => {
  expect(endOfInputParser({ input: 'foo bar baz', index: 0 })).toEqual(
    Failure({
      input: 'foo bar baz',
      index: 0,
      expected: END_OF_INPUT,
      actual: 'foo bar baz',
    })
  );
});

test('the endOfInputParser parser fails if index is not end of input with non-zero index', () => {
  expect(endOfInputParser({ input: 'foo bar baz', index: 8 })).toEqual(
    Failure({
      input: 'foo bar baz',
      index: 8,
      expected: END_OF_INPUT,
      actual: 'baz',
    })
  );
});
