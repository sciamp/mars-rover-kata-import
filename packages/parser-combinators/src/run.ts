import { Parser } from './parser';
import { Context, ParserResult } from './parser-result';

const makeInitialContextFromInput = (input: string): Context => ({
  input,
  index: 0,
});

export const run = <T>(parser: Parser<T>) => (input: string): ParserResult<T> =>
  parser(makeInitialContextFromInput(input));
