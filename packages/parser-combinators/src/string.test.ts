import { Failure, Success } from './parser-result';
import { stringParser } from './string';

test('string parser returns success when input matches the expected string', () => {
  const match = 'expected';
  const parser = stringParser(match);
  expect(
    parser({ input: 'expected is actually in the input', index: 0 })
  ).toEqual(
    Success({
      input: 'expected is actually in the input',
      index: 8,
      value: match,
    })
  );
});

test('string parser returns success when input matches the expected string with non-zero index', () => {
  const match = 'actually';
  const parser = stringParser(match);
  expect(
    parser({ input: 'expected is actually in the input', index: 12 })
  ).toEqual(
    Success({
      input: 'expected is actually in the input',
      index: 20,
      value: match,
    })
  );
});

test("string parser return failure when input doesn't match the expected string", () => {
  const match = 'expected';
  const parser = stringParser(match);
  expect(parser({ input: "this doesn't match", index: 0 })).toEqual(
    Failure({
      input: "this doesn't match",
      index: 0,
      expected: match,
      actual: "this doesn't match",
    })
  );
});

test("string parser return failure when input doesn't match the expected string with non-zero index", () => {
  const match = 'expected';
  const parser = stringParser(match);
  expect(parser({ input: "foo this doesn't match", index: 4 })).toEqual(
    Failure({
      input: "foo this doesn't match",
      index: 4,
      expected: match,
      actual: "this doesn't match",
    })
  );
});
