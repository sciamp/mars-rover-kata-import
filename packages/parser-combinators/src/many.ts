import { Parser } from './parser';
import { Context, ParserResult, ParserSuccess, Success } from './parser-result';

const innerLoop = <T>(
  parser: Parser<T>,
  context: Context,
  result: ParserResult<T[]>,
  previousResult: ParserResult<T[]>
): ParserResult<T[]> =>
  result.match(
    ({ index: lastIndex, value: lastValues }: ParserSuccess<T[]>) => {
      const nextResult = parser({ ...context, index: lastIndex }).flatMap(
        ({ index, value }: ParserSuccess<T>) =>
          Success({
            ...context,
            index,
            value: [...lastValues, value],
          })
      );
      return innerLoop(parser, context, nextResult, result);
    },
    () => previousResult
  );

const many = <T>(parser: Parser<T>): Parser<T[]> => (
  context: Context
): ParserResult<T[]> => {
  return innerLoop(
    parser,
    context,
    Success({ ...context, value: [] }),
    Success({ ...context, value: [] })
  );
};

export { many };
