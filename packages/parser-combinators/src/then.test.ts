import {
  nonNegativeIntegerParser,
  NON_NEGATIVE_INTEGER,
} from './non-negative-integer';
import { Parser } from './parser';
import { Failure, Success } from './parser-result';
import { stringParser } from './string';
import { then } from './then';

test('the then parser combinator takes two parsers and return a new parser that applies the first parser, if the first succeed applies the second', () => {
  const parser: Parser<number> = then(
    stringParser('foo'),
    nonNegativeIntegerParser
  );

  expect(parser({ input: 'foo42', index: 0 })).toEqual(
    Success({
      input: 'foo42',
      index: 5,
      value: 42,
    })
  );
});

test('the then parser combinator parser with constant input function fails if the first parser fails', () => {
  const parser: Parser<number> = then(
    stringParser('foo'),
    nonNegativeIntegerParser
  );

  expect(parser({ input: 'bar42', index: 0 })).toEqual(
    Failure({
      input: 'bar42',
      index: 0,
      expected: 'foo',
      actual: 'bar42',
    })
  );
});

test('the then parser combinator fails if the second parser fails', () => {
  const parser: Parser<number> = then(
    stringParser('foo'),
    nonNegativeIntegerParser
  );

  expect(parser({ input: 'foo042', index: 0 })).toEqual(
    Failure({
      input: 'foo042',
      index: 3,
      expected: NON_NEGATIVE_INTEGER,
      actual: '042',
    })
  );
});
