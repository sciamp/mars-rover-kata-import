import { Parser } from './parser';
import { Context, ParserResult, ParserSuccess } from './parser-result';

const then = <T, S>(
  firstParser: Parser<T>,
  secondParser: Parser<S>
): Parser<S> => (context: Context) => {
  const result: ParserResult<T> = firstParser(context);

  return result.flatMap(({ input, index }: ParserSuccess<T>) =>
    secondParser({ input, index })
  );
};

export { then };
