import { Parser } from './parser';
import { Context, Failure, ParserResult, Success } from './parser-result';

const NON_NEGATIVE_INTEGER = 'NON_NEGATIVE_INTEGER';

const nonNegativeIntegerParser: Parser<number> = ({
  input,
  index,
}: Context): ParserResult<number> => {
  const startsWithDigits = new RegExp(/^0(?!\d)|^(?<!0)[1-9]\d*/);

  const inputToConsume = input.slice(index);

  const match = inputToConsume.match(startsWithDigits);

  if (!match)
    return Failure({
      input,
      index,
      expected: NON_NEGATIVE_INTEGER,
      actual: inputToConsume,
    });

  return Success({
    input,
    index: index + match[0].length,
    value: Number(match),
  });
};

export { NON_NEGATIVE_INTEGER, nonNegativeIntegerParser };
