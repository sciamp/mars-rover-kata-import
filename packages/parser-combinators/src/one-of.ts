import { Parser } from './parser';
import { Context, ParserResult } from './parser-result';

const oneOf = <T>(...parsers: Parser<T>[]) => (
  context: Context
): ParserResult<T> => {
  const [initialParser, ...otherParsers] = parsers;

  const initialResult: ParserResult<T> = initialParser(context);

  return otherParsers.reduce(
    (result, parser) => result.or(() => parser(context)),
    initialResult
  );
};

export { oneOf };
