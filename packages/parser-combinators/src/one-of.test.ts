import { oneOf } from './one-of';
import { Failure, Success } from './parser-result';
import { stringParser } from './string';

test('the oneOf parser combinator successfully parse with the first parser', () => {
  const charA = stringParser('A');
  const charB = stringParser('B');
  const oneOfAB = oneOf(charA, charB);
  expect(oneOfAB({ input: 'ASD', index: 0 })).toEqual(
    Success({
      input: 'ASD',
      index: 1,
      value: 'A',
    })
  );
});

test('the oneOf parser combinator successfully parse with the second parser', () => {
  const charA = stringParser('A');
  const charB = stringParser('B');
  const oneOfAB = oneOf(charA, charB);
  expect(oneOfAB({ input: 'BASD', index: 0 })).toEqual(
    Success({
      input: 'BASD',
      index: 1,
      value: 'B',
    })
  );
});

test('the oneOf parser combinator fails when no parser matches', () => {
  const charA = stringParser('A');
  const charB = stringParser('B');
  const oneOfAB = oneOf(charA, charB);
  expect(
    oneOfAB({ input: 'FOO', index: 0 }).or(error =>
      Failure({ ...error, expected: 'one of A, B' })
    )
  ).toEqual(
    Failure({
      input: 'FOO',
      index: 0,
      expected: 'one of A, B',
      actual: 'FOO',
    })
  );
});
