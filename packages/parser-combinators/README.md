# @mars-rover/parser-combinators
The `@mars-rover/parser-combinators` package is a collection of basic parsers and "higher order parsers" to combine the former to build new parsers.

## parser interface
The state of the parsing process is made up of string (`input`) that represents the string we are parsing and a number (`index`) that represent the point in the input string where the parser stopped:
```ts
interface Context {
  input: string;
  index: number;
}
```
A successful parse operation yields a `ParserSuccess`:
```ts
interface ParserSuccess<T> extends Context {
  value: T;
}
```
On the other hand, when parsing fails we got a `ParserError`:
```ts
interface ParserError extends Context {
  expected: string;
  actual: string;
}
```
This package relies on [@mars-rover/result](/packages/result) package to define the `ParserResult` type:
```ts
type ParserResult<T> = Result<ParserSuccess<T>, ParserError>;
```
There are two helpers function to build, respectively, a success and an error:
```ts
const Success = <T>(value: ParserSuccess<T>): ParserResult<T> =>
  OK<ParserSuccess<T>, ParserError>(value);

const Failure = <T>(error: ParserError): ParserResult<T> =>
  ERR<ParserSuccess<T>, ParserError>(error);
```

A parser it's just a function that takes a `Context` and returns a `ParserResult`
```ts
type Parser<T> = (context: Context) => ParserResult<T>;
```

## parsers

### string
The `stringParsers` takes a string literal and returns a parser that matches that string:
```ts
const fooParser: Parser<string> = stringParser('foo');
const result: ParserResult<string> = fooParser({
  input: 'foo bar',
  index: 0
})                                            // -> Success({ input: 'foo bar', index: 3, value: 'foo' })
```

### non-negative integer
The `nonNegativeIntegerParser` matches non-negative integers:
```ts
const result: ParserResult<number> = nonNegativeIntegerParser({
  input: 'foo 42',
  index: 4
})                                            // -> Success({ input: 'foo 42', index: 6, value: 42 })
```

### end of input
The `endOfInputParser` parser succeeds if the input has beed successfully consumed

## combinators
Combinators are functions that takes a given parser (or more than one) and returns a new parser.

### bind
The `bind` combinator takes a parser and a function that transform a successful result of the given parser in a new parser:
```ts
// This is the main parser, we are going to attempt parsing the input string with this one.
const mainParser: Parser<string> = stringParser('foo');

// If the main parser succeeds than we want to run the boundParser that will attempt matching a
// non-negative integer and, if it succeeds, will combine the result from the main parser with
// its own result
const boundParser = (matchedValue: string): Parser<{ [key: string]: number }> => (context: Context) =>
  nonNegativeIntegerParser(context).map(({ value, ...nextContext }) => ({
    ...nextContext,
    value: { [matchedValue]: value },
  }));

const parser: Parser<{ [key: string]: number }> = bind(
  mainParser,
  boundParser
);

parser({ input: 'foo42', index: 0 }) // -> Success({ input: 'foo42', index: 5, value: { foo: 42 } })
```

### then
The `then` combinator is very similar to the `bind` combinator but discards the result from the first parser:
```ts
const parser: Parser<number> = then(
  stringParser('foo'),
  nonNegativeIntegerParser
);

parser({ input: 'foo42', index: 0 }) // -> Success({ input: 'foo42', index: 5, value: 42 })
```

### one of
The `oneOf` combinator takes a list of parser and returns a parser that succeeds if one of the given parser succeeds 
```ts
const charA = stringParser('A');
const charB = stringParser('B');
const oneOfAB = oneOf(charA, charB);

oneOfAB({ input: 'BAR', index: 0 }) // -> Success({ input: 'BAR', index: 1, value: 'B' })
```

### many
The `many` combinator takes a parser and attempt matching it as much as possible:
```ts
const helloParser: Parser<string> = stringParser('hello!');
const manyParser: Parser<string[]> = many(helloParser);

manyParser({
  input: 'hello!hello!hello! foo bar',
  index: 0
})         // -> Success({ input: 'hello!hello!hello! foo bar', index: 18, value: ['hello!', 'hello!', 'hello!'] })
```
Please note that a parser obtained from another one using the `many` combinator will **never** fail, if there are no matches it will return an empty successful result:
```ts
Success<T[]>({ input: '...', index: 0, value: [] })
```

#### known limitations
Parsing more than 2000 repetitions (tested with 3000) results in `RangeError: Maximum call stack size exceeded`

### with error
The `withError` combinator takes a parser and a function that transform a `ParserError` into another one:
```ts
const fooParser = stringParser('foo');

const parserErrorYeller = withError(parser, error => ({
  ...error,
  expected: 'HEY MAN! I AIN\'T GOT A FOO!',
}));

parserErrorYeller({
  input: 'fo bar',
  index: 0
}) // -> Failure({ input: 'fo bar', index: 0, expected: 'HEY MAN! I AIN\'T GOT A FOO!', actual: 'fo bar' })
