import { Point } from './point';

export interface BoundingBox {
  includes: (point: Point) => boolean;
}

export const boundingBox = (p1: Point, p2: Point): BoundingBox => ({
  includes: (point: Point) => {
    const xIsInRange =
      point.x >= Math.min(p1.x, p2.x) && point.x <= Math.max(p1.x, p2.x);
    const yIsInRange =
      point.y >= Math.min(p1.y, p2.y) && point.y <= Math.max(p1.y, p2.y);

    return xIsInRange && yIsInRange;
  },
});
