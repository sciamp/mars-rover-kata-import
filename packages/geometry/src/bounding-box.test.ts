import { boundingBox } from './bounding-box';
import { point } from './point';

test('"boundingBox" is a function', () => {
  expect(typeof boundingBox).toBe('function');
});

test('the "boundingBox" function takes two points and returns an object', () => {
  expect(typeof boundingBox(point(-4, 5), point(3, 29))).toBe('object');
});

test('the "boundingBox.contains" function returns true if the given point is in the bounding box', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(4, 6))).toBe(
    true
  );
});

test('the "boundingBox.contains" function returns true if the given point is on the eastern boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(7, 0))).toBe(
    true
  );
});

test('the "boundingBox.contains" function returns true if the given point is on the western boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(3, 0))).toBe(
    true
  );
});

test('the "boundingBox.contains" function returns true if the given point is on the northern boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(4, 7))).toBe(
    true
  );
});

test('the "boundingBox.contains" function returns true if the given point is on the southern boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(5, -5))).toBe(
    true
  );
});

test('the order of the bounding box points is not relevant when checking if a point is inside the bounding box', () => {
  expect(boundingBox(point(7, 7), point(3, -5)).includes(point(4, 6))).toBe(
    true
  );
});

test('the "boundingBox.contains" function returns false if the given point is east of the eastern boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(8, 0))).toBe(
    false
  );
});

test('the "boundingBox.contains" function returns false if the given point is west of the western boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(0, 0))).toBe(
    false
  );
});

test('the "boundingBox.contains" function returns false if the given point is north of the northern boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(4, 8))).toBe(
    false
  );
});

test('the "boundingBox.contains" function returns false if the given point is south of the southern boarder', () => {
  expect(boundingBox(point(3, -5), point(7, 7)).includes(point(5, -6))).toBe(
    false
  );
});
