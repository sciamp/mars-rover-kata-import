export interface Point {
  readonly x: number;
  readonly y: number;
}

export const point = (x: number, y: number): Point => ({
  get x() {
    return x;
  },
  get y() {
    return y;
  },
});
