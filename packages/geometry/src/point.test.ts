import { point } from './point';

test('"point" is a function', () => {
  expect(typeof point).toBe('function');
});

test('the "point" function takes two numbers and returns an object', () => {
  expect(typeof point(3, 7)).toBe('object');
});

test('the point has a "x" attribute to read the x coordinate', () => {
  expect(point(3, 7).x).toBe(3);
});

test('the point has a "y" attribute to read the y coordinate', () => {
  expect(point(3, 7).y).toBe(7);
});
