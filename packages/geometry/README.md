# @mars-rover/geometry

The `@mars-rover/geometry` package is a small collection of geometric entities and helpers

## point
The `Point` interface represents a two-dimensional point with cartesian coordinates:
```ts
interface Point {
  readonly x: number;
  readonly y: number;
}
```
The `point` helper builds a new `Point` given an `x` and a `y` coordinates:
```ts
const newPoint = point(42, 1000);
newPoint.x; // -> 42
newPoint.y; // -> 1000
```

## bounding box
The `BoundingBox` interface represents a portion of the cartesian plane identified by two opposite vertices of a rectangle, given a `Point` it can tell if it is included or not in that area:
```ts
interface BoundingBox {
  includes: (point: Point) => boolean;
}
```
The `boundingBox` helper takes two `Point`s and creates a `BoundingBox`:
```ts
const bb = boundingBox(point(3, -5), point(7, 7));
bb.includes(point(4, 6)); // -> true
```
