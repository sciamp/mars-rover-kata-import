# @mars-rover/result
The `@mars-rover/result` package provides an interface to represent operation results and to perform other operations on it:
```ts
abstract class Result<SuccessType, ErrorType> {
  readonly value?: SuccessType;

  readonly error?: ErrorType;

  public abstract match<T>(
    ok: (result: SuccessType) => T,
    err: (result: ErrorType) => T
  ): T;

  public abstract map<T>(
    transform: (result: SuccessType) => T
  ): Result<T, ErrorType>;

  public abstract flatMap<T>(
    transform: (result: SuccessType) => Result<T, ErrorType>
  ): Result<T, ErrorType>;

  public abstract or(
    fallback: (result: ErrorType) => Result<SuccessType, ErrorType>
  ): Result<SuccessType, ErrorType>;
}
```
There are two classes that implement this interface: `OkResult` for successful results and `ErrorResult` to represent results from non-successful operations.

There are also two helpers function to build, respectively, an `OkResult` and an `ErrorResult`:
```ts
const OK: <SuccessType, ErrorType>(value: SuccessType) => OkResult<SuccessType, ErrorType>
```
```ts
const ERR: <SuccessType, ErrorType>(error: ErrorType) => ErrorResult<SuccessType, ErrorType>
```

The `match` method takes two functions and applies the first one if `Result` is `OkResult` and the second one otherwise:

```ts
OK('yay!').match(
  success => `got: #{result}`,
  error => `something bad happened: #{error}`
)                                             // -> got: yay!
ERR('F@*#!').match(
  success => `got: #{result}`,
  error => `something bad happened: #{error}`
)                                             // -> something bad happened: F@*#!
```

The `map` method applies a given function to the result only if it is successful and wrap the result in a `Result`
```ts
const result: Result<string, any> = OK('yay!');
const mappedResult: Result<{ isSuccess: boolean, message: string }> = result.map(
  success => ({ isSuccess: true, message: success })
)                                             // -> { isSuccess: true, message: 'yay!' }
```

The `flatMap` method is pretty much the same of `map` but it's more convenient when the given function can in turn succeed or fail (and hence returning something wrapped in a `Result`). Using the `map` method in this case would mean dealing at some point with a `Result<Result<T, ErrorType>, ErrorType>`:
```ts
interface User {
  id: number;
  email: string;
}

interface Post {
  authorId: number;
  title: string;
  body: string;
}

interface Error {
  message: string;
}

declare function loadUserById(id: number): Result<User, Error>;
declare function loadUserPosts(authorId: number): Result<Post[], Error>;

const user: Result<User, Error> = loadUserById(42);
const posts: Result<Post[], Error> = user.flatMap(({ id }) => loadUserPosts(id));
```

The `or` method applies a given fallback function when the result is not successful. It's basically the same that `flatMap` but for errors.
