/* eslint-disable @typescript-eslint/no-unused-vars */

import { MapFun, MatchFun, Result } from './result';

class OkResult<SuccessType, ErrorType> extends Result<SuccessType, ErrorType> {
  readonly value: SuccessType;

  static makeOkResult<S, E>(value: S): OkResult<S, E> {
    return new OkResult(value);
  }

  protected constructor(value: SuccessType) {
    super();
    this.value = value;
  }

  public match<T>(ok: MatchFun<SuccessType, T>, _err: MatchFun<any, T>): T {
    return ok(this.value);
  }

  public map<T>(transform: MapFun<SuccessType, T>): Result<T, ErrorType> {
    return OkResult.makeOkResult(transform(this.value));
  }

  public flatMap<T>(
    transform: MapFun<SuccessType, Result<T, ErrorType>>
  ): Result<T, ErrorType> {
    return transform(this.value);
  }

  public or(
    _fallback: MapFun<ErrorType, Result<SuccessType, ErrorType>>
  ): Result<SuccessType, ErrorType> {
    return this;
  }
}

const OK = OkResult.makeOkResult;

export { OkResult, OK };
