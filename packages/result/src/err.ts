/* eslint-disable @typescript-eslint/no-unused-vars */

import { MapFun, MatchFun, Result } from './result';

class ErrorResult<SuccessType, ErrorType> extends Result<
  SuccessType,
  ErrorType
> {
  readonly error: ErrorType;

  static makeErrorResult<S, E>(error: E): ErrorResult<S, E> {
    return new ErrorResult(error);
  }

  protected constructor(error: ErrorType) {
    super();
    this.error = error;
  }

  public match<T>(
    _ok: MatchFun<SuccessType, T>,
    err: MatchFun<ErrorType, T>
  ): T {
    return err(this.error);
  }

  public map<T>(_transform: MapFun<any, T>): Result<T, ErrorType> {
    return ErrorResult.makeErrorResult<T, ErrorType>(this.error);
  }

  public flatMap<T>(
    _transform: MapFun<SuccessType, Result<T, ErrorType>>
  ): Result<T, ErrorType> {
    return ErrorResult.makeErrorResult<T, ErrorType>(this.error);
  }

  public or(
    fallback: MapFun<ErrorType, Result<SuccessType, ErrorType>>
  ): Result<SuccessType, ErrorType> {
    return fallback(this.error);
  }
}

const ERR = ErrorResult.makeErrorResult;

export { ErrorResult, ERR };
