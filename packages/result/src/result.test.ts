import { ERR } from './err';
import { OK } from './ok';
import { MatchFun, Result } from './result';

test('the ERR helper function returns an ErrorResult', () => {
  const err = ERR('something nasty happened!');

  expect(err.value).toBeUndefined();
  expect(err.error).toBe('something nasty happened!');
});

test('the OK helper function return an OkResult', () => {
  const ok = OK('some value from a successful operation');

  expect(ok.value).toBe('some value from a successful operation');
  expect(ok.error).toBeUndefined();
});

test('the match method runs the ok callback on OkResult', () => {
  type CallbackLog = { func: string; input: any[] };
  const okLog: CallbackLog[] = [];
  const errLog: CallbackLog[] = [];

  const okCallback: MatchFun<string, string> = (result: string) => {
    okLog.push({ func: 'okCallback', input: [result] });
    return `OK: "${result}"`;
  };

  const errCallback: MatchFun<string, string> = (result: string) => {
    errLog.push({ func: 'errCallback', input: [result] });
    return `ERROR: "${result}"`;
  };

  const result: Result<string, string> = OK(
    'some value from a successful operation'
  );

  const matchResult = result.match(
    value => okCallback(value),
    error => errCallback(error)
  );

  expect(matchResult).toBe('OK: "some value from a successful operation"');
  expect(okLog.length).toBe(1);
  expect(okLog[0]).toEqual({
    func: 'okCallback',
    input: ['some value from a successful operation'],
  });
  expect(errLog.length).toBe(0);
});

test('the match method runs the err callback on ErrorResult', () => {
  type CallbackLog = { func: string; input: any[] };
  const okLog: CallbackLog[] = [];
  const errLog: CallbackLog[] = [];

  const okCallback: MatchFun<string, string> = (result: string) => {
    okLog.push({ func: 'okCallback', input: [result] });
    return `OK: "${result}"`;
  };

  const errCallback: MatchFun<string, string> = (result: string) => {
    errLog.push({ func: 'errCallback', input: [result] });
    return `ERROR: "${result}"`;
  };

  const result: Result<string, string> = ERR('something nasty happened!');

  const matchResult = result.match(
    value => okCallback(value),
    error => errCallback(error)
  );

  expect(matchResult).toBe('ERROR: "something nasty happened!"');
  expect(errLog.length).toBe(1);
  expect(errLog[0]).toEqual({
    func: 'errCallback',
    input: ['something nasty happened!'],
  });
  expect(okLog.length).toBe(0);
});

test('the map method from OkResult applies a transform function to the value and wrap it into a result', () => {
  type CallbackLog = { func: string; input: any[] };
  const log: CallbackLog[] = [];

  const result: Result<string, string> = OK(
    'some value from a successful operation'
  );

  const mapResult = result.map(value => {
    log.push({ func: 'transform', input: [value] });
    return { foo: 'bar', value };
  });

  expect(mapResult.value).toEqual({
    foo: 'bar',
    value: 'some value from a successful operation',
  });
  expect(mapResult.error).toBeUndefined();
  expect(log.length).toBe(1);
  expect(log[0]).toEqual({
    func: 'transform',
    input: ['some value from a successful operation'],
  });
});

test('the map method from ErrorResult returns the receiver', () => {
  type CallbackLog = { func: string; input: any[] };
  const log: CallbackLog[] = [];

  const result: Result<string, string> = ERR('something nasty happened!');

  const mapResult = result.map(value => {
    log.push({ func: 'transform', input: [value] });
    return { foo: 'bar', value };
  });

  expect(mapResult.value).toBeUndefined();
  expect(mapResult.error).toBe('something nasty happened!');
  expect(log.length).toBe(0);
});

test(
  'the flatMap method from OkResult applies a transform function that returns Result to the value and return an' +
    ' OkResult if the transform is successful',
  () => {
    type CallbackLog = { func: string; input: any[] };
    const log: CallbackLog[] = [];

    const result: Result<number, string> = OK(42);

    const flatMapResult: Result<string, string> = result.flatMap(value => {
      log.push({ func: 'transform', input: [value] });
      return OK(
        `The Answer to the Ultimate Question of Life, the Universe, and Everything is ${value}`
      );
    });

    expect(flatMapResult.value).toEqual(
      'The Answer to the Ultimate Question of Life, the Universe, and Everything is 42'
    );
    expect(flatMapResult.error).toBeUndefined();
    expect(log.length).toBe(1);
    expect(log[0]).toEqual({
      func: 'transform',
      input: [42],
    });
  }
);

test(
  'the flatMap method from OkResult applies a transform function that returns Result to the value and return an' +
    ' ErrResult if the transform is not successful',
  () => {
    type CallbackLog = { func: string; input: any[] };
    const log: CallbackLog[] = [];

    const result: Result<number, string> = OK(42);

    const flatMapResult: Result<string, string> = result.flatMap(value => {
      log.push({ func: 'transform', input: [value] });
      return ERR('something nasty happened!');
    });

    expect(flatMapResult.value).toBeUndefined();
    expect(flatMapResult.error).toBe('something nasty happened!');
    expect(log.length).toBe(1);
    expect(log[0]).toEqual({
      func: 'transform',
      input: [42],
    });
  }
);

test(
  'the flatMap method from ErrorResult applies a transform function that returns Result to the value and return the' +
    ' initial ErrResult when the transform is successful',
  () => {
    type CallbackLog = { func: string; input: any[] };
    const log: CallbackLog[] = [];

    const result: Result<string, string> = ERR('something nasty happened!');

    const flatMapResult: Result<number, string> = result.flatMap(value => {
      log.push({ func: 'transform', input: [value] });
      return OK(42);
    });

    expect(flatMapResult.value).toBeUndefined();
    expect(flatMapResult.error).toBe('something nasty happened!');
    expect(log.length).toBe(0);
  }
);

test(
  'the flatMap method from ErrorResult applies a transform function that returns Result to the value and return the' +
    ' initial ErrResult when the transform is not successful',
  () => {
    type CallbackLog = { func: string; input: any[] };
    const log: CallbackLog[] = [];

    const result: Result<string, string> = ERR('something nasty happened!');

    const flatMapResult: Result<number, string> = result.flatMap(value => {
      log.push({ func: 'transform', input: [value] });
      return ERR('something even nastier happened!');
    });

    expect(flatMapResult.value).toBeUndefined();
    expect(flatMapResult.error).toBe('something nasty happened!');
    expect(log.length).toBe(0);
  }
);

test('the or method from OkResult does nothing and returs the receiver', () => {
  type CallbackLog = { funct: string; input: any[] };
  const log: CallbackLog[] = [];

  const result: Result<string, string> = OK(
    'some value from a successful operation'
  );

  const orResult = result.or(error => {
    log.push({ funct: 'orCallback', input: [error] });
    return OK(`Got this error from previous operation: ${error}`);
  });

  expect(orResult.value).toBe('some value from a successful operation');
  expect(orResult.error).toBeUndefined();
  expect(log.length).toBe(0);
});

test('the or method from ErrorResult calls the fallback function, if the fallback function succeed returns an OkResult', () => {
  type CallbackLog = { funct: string; input: any[] };
  const log: CallbackLog[] = [];

  const result: Result<string, string> = ERR('something nasty happened!');

  const orResult = result.or(error => {
    log.push({ funct: 'orCallback', input: [error] });
    return OK(`Got this error from previous operation: ${error}`);
  });

  expect(orResult.value).toBe(
    'Got this error from previous operation: something nasty happened!'
  );
  expect(orResult.error).toBeUndefined();
  expect(log.length).toBe(1);
  expect(log[0].funct).toBe('orCallback');
  expect(log[0].input).toEqual(['something nasty happened!']);
});

test('the or method from ErrorResult calls the fallback function, if the fallback function fails returns an ErrorResult', () => {
  type CallbackLog = { funct: string; input: any[] };
  const log: CallbackLog[] = [];

  const result: Result<string, string> = ERR('something nasty happened!');

  const orResult = result.or(error => {
    log.push({ funct: 'orCallback', input: [error] });
    return ERR('something even nastier happened!');
  });

  expect(orResult.value).toBeUndefined();
  expect(orResult.error).toBe('something even nastier happened!');
  expect(log.length).toBe(1);
  expect(log[0].funct).toBe('orCallback');
  expect(log[0].input).toEqual(['something nasty happened!']);
});
