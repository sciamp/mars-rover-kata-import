type MatchFun<T, S> = (result: T) => S;
type MapFun<T, S> = (value: T) => S;

abstract class Result<SuccessType, ErrorType> {
  readonly value?: SuccessType;

  readonly error?: ErrorType;

  public abstract match<T>(
    ok: MatchFun<SuccessType, T>,
    err: MatchFun<ErrorType, T>
  ): T;

  public abstract map<T>(
    transform: MapFun<SuccessType, T>
  ): Result<T, ErrorType>;

  public abstract flatMap<T>(
    transform: MapFun<SuccessType, Result<T, ErrorType>>
  ): Result<T, ErrorType>;

  public abstract or(
    fallback: MapFun<ErrorType, Result<SuccessType, ErrorType>>
  ): Result<SuccessType, ErrorType>;
}

export { Result, MatchFun, MapFun };
